function Neural(SET, NeuroE, e) {
	var _this=this,
		test=0,
		master_match=[],
		bests=[{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0}],
		revs=0,
		done,stopIt = false,
		master_count=0,
		end_length=0,
		testNo=0;
	this.ends={'in':[],'out':[]}
	this.construct=function () {
		if (e===undefined)
			this.find();
		else this.build();
		this.NEURO=new Neuro('new',true,NeuroE)
		this.T_NEURO=new Neuro('new')
		this.T_NEURO.run_limit=200
		this.NEURO.run_limit=200
		this.NEURO.run_delay=100
		this.NEURO.draw()
		this.loadSet()
		this.parts_list()
	}
	this.loadSet=function () {
		if (typeof SET == 'string' && window.test_parts.length) {
			for (var t in window.test_parts) {
				if (window.test_parts[t].name == SET) {
					SET = window.test_parts[t];
					break;
				}
			}
		}
		if (typeof SET == 'object') {
			testNo=SET.runs.length;
			end_length = 0;
			this.ends={'in':[],'out':[]}
			for (var i=0;i<SET.inputs;i++) {
				end_length++
				this.ends['in'].push(this.NEURO.neurons.length)
				this.NEURO.add_neuron('O')()
				this.T_NEURO.add_neuron('O')()
			}
			for (var o=0;o<SET.outputs;o++) {
				end_length++
				this.ends['out'].push(this.NEURO.neurons.length)
				this.NEURO.add_neuron('R')()
				this.T_NEURO.add_neuron('R')()
			}
			this.set_inputs()
			this.tests_list()
		}
	}
	this.clean=function() {
		if (_this.clean_kind=='link' && _this.n<0) {
			_this.ret()
			_this.stop()
			stopIt = false
			done = undefined
			return
		} else
		if (_this.clean_kind=='neuron' && _this.n<end_length) {
			_this.clean_kind='link'
			_this.n=_this.NEURO.links.length-1
		}
		var dec=_this.decide({'kind':_this.clean_kind,'action':'remove','reff':_this.n})
		_this.n--
		//console.log(dec)
		var UNDO=_this.revise(dec)
		var tested=_this.tests()
		//console.log(tested)
		if (tested.count==SET.out.length*SET.outputs) {
			_this.NEURO.run()
			window.setTimeout(_this.clean,1000)
		} else {
			_this.NEURO.draw()
			window.setTimeout(_this.un_clean(UNDO),500)
		}

	}
	this.clean_up=function() {
		console.log('-----------------clean up')
		_this.n=this.NEURO.neurons.length-1
		_this.clean_kind='neuron'
		_this.clean()
		//return this.NEURO
	}
	this.set_T=function() {
		this.T_NEURO.neurons=this.T_NEURO.load_neurons(this.NEURO.save_neurons())
		this.T_NEURO.links=clone(this.NEURO.links)
	}
	this.decide=function (force) {
		/* These are exactly the
		kind of rules that could
		be decided by the neurons */
		if (force===undefined) force={}
		var decision={'kind':false,'action':false,'reff':false}
		//force & decide kind
		for (var d in decision) {
			if (d in force){
				decision[d]=force[d]
			} else {
				if (d=='kind') {
					decision[d]=rand_i(['neuron','link'])
				} else if (d=='action') {
					decision[d]=rand_int(1,10)
				}
			}
		}

		//decide action
		if (decision['kind']=='link' && decision['action']==10) decision['action']='add'
		if (decision['action']==10 && this.NEURO.get_type('V').length==0) decision['action']='remove'
		if (decision['action']<=5) decision['action']='add'
		else if (decision['action']<=9) decision['action']='remove'
		else if (decision['action']==10) decision['action']='change'
		if (decision['kind']=='neuron') {
			if (decision['action']=='remove' && this.NEURO.neurons.length<(end_length+1)) {
				decision['action']=rand_int(1,10)
				if (decision['action']<3) decision['action']='remove'
				else decision['action']='add'
			}
			if (decision['action']=='add' && this.NEURO.neurons.length>20) {
				decision['action']=rand_int(1,10)
				if (decision['action']<3) decision['action']='add'
				else decision['action']='remove'
			}
			if (decision['action']=='remove' && this.NEURO.neurons.length==end_length) decision['action']='add'
		} else if (decision['kind']=='link') {
			if (decision['action']=='remove' && this.NEURO.links.length<end_length) {
				decision['action']=rand_int(1,10)
				if (decision['action']<3) decision['action']='remove'
				else decision['action']='add'
			}
			if (decision['action']=='add' && this.NEURO.links.length>(this.NEURO.neurons.length*10)) {
				decision['action']=rand_int(1,10)
				if (decision['action']<3) decision['action']='add'
				else decision['action']='remove'
			}
			if (decision['action']=='remove' && this.NEURO.links.length==0) decision['action']='add'
		}

		//set reff
		if(!('reff' in force)){
			if (decision['kind']=='link') {
				//if add
				if (decision['action']=='add') {
					var x_link
					//choose output from empty
					var out_link
					var out_links=[]
					for (var n in this.NEURO.neurons) {
						for (var e in this.NEURO.neurons[n].ends['out']) {
							var nl=this.NEURO.next_link([parseInt(n),'out',parseInt(e)]);
							if (! nl) {
								out_links.push([parseInt(n),'out',parseInt(e)])

							}
						}
					}
					//choose input from any
					var in_link
					var in_links=[]
					for (var n in this.NEURO.neurons) {
						for (var e in this.NEURO.neurons[n].ends['in']) {
							in_links.push([parseInt(n),'in',parseInt(e)])
						}
					}
					x_link = _this.get_link(out_links, in_links)

					in_link=x_link.in
					out_link=x_link.out
					//add
					if (in_link===undefined || out_link===undefined) {
						return false
					} else {
						decision['reff']=[in_link,out_link]
					}
				//else
				} else if (decision['action']=='remove') {
					//select random
					decision['reff']=rand_int(0,this.NEURO.links.length-1)
					//remove
				}
			//if neuron
			} else if (decision['kind']=='neuron') {
				//if add
				if (decision['action']=='add') {
					//add random (not O/R)
					var types=[]
					for (var v in window.neuron_defs) {
						if (v!='O'&&v!='R'&&v!='N') {
							types.push(v)
						}
					}
					decision['reff']=rand_i(types)
				//if remove
				} else if (decision['action']=='remove') {
					//select neuron (not O/R)
					var neurons=[]
					for (var n in this.NEURO.neurons){
						var neuron=this.NEURO.neurons[n]
						if(neuron.type!='O'&&neuron.type!='R'){
							neurons.push(n)
						}
					}
					//remove
					if (neurons.length) {
						decision['reff']=rand_i(neurons)
					}
				//if change
				} else if (decision['action']=='change') {
					//select neuron (V)
					var vs=this.NEURO.get_type('V'),
						n=rand_i(vs);
					decision['reff']=n
					decision['value']=rand_int(0,1)
				}
			}
		}


		return decision
	}
	this.find=function () {
		// this.$panel=$('<aside/>',{'class':'neural_contain well'})
			// this.$main=$('<div/>',{'class':'form-group form-group-sm'})
		this.$neurals = $('.neural_parts')
				this.$name=$('.neural_name')
					this.$run=$('.neural_run')
					this.$reTest=$('.neural_newtest')
					this.$clear=$('.neural_clear')
					this.$save=$('.neural_save')
					this.$new=$('.neural_new')
				this.$inputs_input=	 $('#neural_inputs');
				this.$outputs_input= $('#neural_outputs')
			this.$addCase=$('.add_test_case')
			this.$tests=$('.neural_tests_list')
			this.$testCases=$('.neural_test_cases')

		this.events()
	}
	this.parts_list = function () {
		_this.$neurals.empty()
		for(var sp in window.test_parts) {
			if ('A' in window) {
				var $part=window.A.render($('#neural-part-template').html(),_this.$neurals,'dom'),
						$part_name=$part.find('.part_name'),
						$part_delete=$part.find('.part_delete');
				$part_name.find('span.neural_label').html(window.test_parts[sp]['name']);
				$part_name.find('span.badge').html(window.test_parts[sp]['runs'].length);
			} else {
				var $part=$('<div/>',{'class':'part btn-group btn-group-sm',title:'edit'}),
						$part_name=$('<div/>',{'class':'part_name btn btn-default'}).html(window.test_parts[sp]['name']),
						$part_delete=$('<div/>',{'class':'part_delete btn btn-danger',title:'delete'}).html("<i class='glyphicon glyphicon-remove'></i>");
				_this.$neurals
					.append($part
						.append($part_name)
						.append($part_delete))
			}

			$part_name.click(_this.load_part(window.test_parts[sp].id))
			$part_delete.dblclick(_this.delete_part(sp))
		}
	}
	this.load_test = function (sp) {
		return function () {
			testNo=sp
			var load = SET.runs[sp].afterClean || SET.runs[sp].beforeClean
			_this.NEURO.neurons=_this.NEURO.load_neurons(load.neurons)
			_this.NEURO.links=clone(load.links)
			_this.set_T();
			_this.NEURO.draw();
		}
	}
	this.tests_list = function () {
		_this.$tests.empty()
		for(var sp in SET.runs) {
			if ('A' in window) {
				var $test=window.A.render($('#neural-test-template').html(),_this.$tests,'dom'),
						$test_name=$test.find('.test_name');
						// $test_delete=$test.find('.test_delete');
				// $test_name.find('span.neural_label').html(window.test_parts[sp]['name']);
				$test_name.html(sp);

			$test.click(_this.load_test(sp))
			// $test_delete.dblclick(_this.delete_test(sp))
			}
		}
	}
	this.load_neuro = function () {
		_this.set_T();
		SET={
			"name": _this.NEURO.part_name,
			"inputs":_this.NEURO.get_type('O').length,
			"outputs":_this.NEURO.get_type('R').length,
			"in": [],
			"out": [],
			"runs":[]
		};
		testNo=0;
		end_length = SET.inputs+SET.outputs;
		test=0
		master_match=[]
		bests=[{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0}]
		revs=0
		this.ends={'in':_this.NEURO.get_type('O'),'out':_this.NEURO.get_type('R')}

		this.set_inputs()
		this.tests_list()
	}
	this.next_part_id=function () {
		var found=false,
			id=0;
		while (! found) {
			var found_cur=false;
			for (var p in window.test_parts) {
				if (window.test_parts[p]['id']==id) {
					found_cur=true
					break;
				}
			}
			if (!found_cur) return id;
			else id++
		}
	}
	this.delete_part=function (sp) {
		return function () {
			window.test_parts.splice(sp,1)
			localStorage.setItem('test_parts', JSON.stringify(window.test_parts));
			_this.parts_list()
		}
	}
	this.new = function () {
		_this.set_T();
		SET={
			"name":'new test',
			"inputs":1,
			"outputs":1,
			"in": [],
			"out": [],
			"runs":[]
		};
		testNo=0;
		end_length = SET.inputs+SET.outputs;
		test=0
		master_match=[]
		bests=[{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0}]
		revs=0
		this.ends={'in':_this.NEURO.get_type('O'),'out':_this.NEURO.get_type('R')}

		this.set_inputs()
		this.tests_list()
	}
	this.load_part=function(sp) {
		return function () {
			if (SET.id == window.test_parts[sp].id)
				return;
			SET = window.test_parts[sp];
			_this.NEURO.unload()
			_this.T_NEURO.unload()
			_this.construct()
			// _this.part=sp
			// _this.construct()
			// _this.$fileinfo.html($('#fileinfo-template').html())
		}
	}
	this.build=function() {
		e.empty()
		this.$panel=$('<aside/>',{'class':'neural_contain well'})
			this.$main=$('<div/>',{'class':'form-group form-group-sm'})
				this.$name=$('<input/>',{'class':'neural_name form-control'})
				this.$runs=$('<div/>',{'class':' btn-group btn-group-sm'})
					this.$run=$('<div/>',{'class':'test-run btn btn-primary'})
					this.$reTest=$('<div/>',{'class':' btn btn-success'}).html("Re-Run")
					this.$clear=$('<div/>',{'class':' btn btn-danger'}).html("Clear")
			this.$inputs=$('<label/>',{
				'class':'input-group input-group-sm',
				for:'neural_inputs'}).html("# Inputs")
				this.$inputs_input=		$('<input/>',{'id':'neural_inputs','class':'form-control',type:'number',min:1,max:4,step:1})
			this.$outputs=$('<label/>',{
				'class':'input-group input-group-sm',
				for:'neural_outputs'}).html("# Outputs")
				this.$outputs_input=	$('<input/>',{'id':'neural_outputs','class':'form-control',type:'number',min:1,max:4,step:1})
			this.$addCase=$('<div/>',{'class':' btn btn-primary fa fa-plus'})
			this.$testCases=$('<section/>',{})
		e	.append(this.$panel
				.append(this.$main
					.append(this.$name)
					.append(this.$runs
						.append(this.$run)
						.append(this.$reTest)
						.append(this.$clear)))
				.append(this.$inputs
					.append(this.$inputs_input))
				.append(this.$outputs
					.append(this.$outputs_input))
				.append(this.$addCase)
				.append(this.$testCases))
		this.set_inputs()
		this.events()
	}
	this.stop = function () {
		_this.$run.removeClass('running')
		stopIt = true
		window.clearTimeout(_this.run_timeout)

	}
	this.start = function () {
		_this.$run.addClass('running')
		stopIt = false
		_this.run_timeout = window.setTimeout(_this.revisions,100)

	}
	this.clear = function () {
		test=0
		master_match=[]
		bests=[{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0},{'count':0}]
		revs=0
		_this.NEURO.clear()
		_this.T_NEURO.clear()
	}
	this.events = function () {
		this.$name.unbind('change').change(function () {
			SET.name = _this.$name.val()
		})
		this.$run.unbind('click').click(function () {
			if (_this.$run.is('.running'))
				_this.stop()
			else _this.start()
		})
		this.$reTest.unbind('click').click(function () {
			if (_this.$run.is('.running'))
				_this.stop()
			_this.clear()
			testNo = SET.runs.length
			// _this.NEURO.part = 'new'
			_this.start()
		})
		this.$clear.unbind('click').click(function () {
			_this.stop()
			_this.clear()
		})
		this.$save.unbind('click').click(function () {
			_this.save()
		})
		this.$new.unbind('click').click(function () {
			_this.new()
		})
		this.$addCase.unbind('click').click(function () {
			var defs = {in:[],out:[]}
			for (var i=0;i<SET.inputs;i++)
				defs.in.push(0)
			for (var i=0;i<SET.outputs;i++)
				defs.out.push(0)
			SET.in.push(defs.in)
			SET.out.push(defs.out)
			_this.set_inputs()
		})
		this.$inputs_input.unbind('change').change(function () {
			var val = parseInt(_this.$inputs_input.val()),
				diff = val-SET.inputs,
				dir = Math.sign(diff);
			SET.inputs = val
			for (var i in SET.in) {
				while (SET.in[i].length < SET.inputs)
					SET.in[i].push(0)
				while (SET.in[i].length > SET.inputs)
					SET.in[i].pop()
			}
			_this.set_inputs()


			for (var i=0; i<diff*dir;i++) {
				var j = i*dir;
				end_length+=dir
				if (dir == 1) {
					_this.ends['in'].push(_this.NEURO.neurons.length)
					_this.NEURO.add_neuron('O')()
					_this.T_NEURO.add_neuron('O')()
				} else if (dir == -1) {
					var I = _this.ends['in'].pop()
					_this.NEURO.remove_neuron(I)
					_this.T_NEURO.remove_neuron(I)
					for (var a in _this.ends) {
						for (var b in _this.ends[a]) {
							if (_this.ends[a][b] > I)
								_this.ends[a][b] -= 1
						}
					}
				}
			}
			_this.NEURO.draw()
		})
		this.$outputs_input.unbind('change').change(function () {
			var val = parseInt(_this.$outputs_input.val()),
				diff = val-SET.outputs,
				dir = Math.sign(diff);
			SET.outputs = val
			for (var i in SET.out) {
				while (SET.out[i].length < SET.outputs)
					SET.out[i].push(0)
				while (SET.out[i].length > SET.outputs)
					SET.out[i].pop()
			}
			_this.set_inputs()

			for (var i=0; i<diff*dir;i++) {
				var j = i*dir;
				end_length+=dir
				if (dir == 1) {
					_this.ends['out'].push(_this.NEURO.neurons.length)
					_this.NEURO.add_neuron('R')()
					_this.T_NEURO.add_neuron('R')()
				} else if (dir == -1) {
					var I = _this.ends['out'].pop()
					_this.NEURO.remove_neuron(I)
					_this.T_NEURO.remove_neuron(I)
					for (var a in _this.ends) {
						for (var b in _this.ends[a]) {
							if (_this.ends[a][b] > I)
								_this.ends[a][b] -= 1
						}
					}
				}
			}
			_this.NEURO.draw()
		})
	}
	this.set_inputs = function () {
		console.log(this.$name,SET.name, SET)
		this.$name.val(SET.name)
		this.NEURO.part_name=SET.name
		this.NEURO.set_inputs()
		this.$inputs_input.val(SET.inputs)
		this.$outputs_input.val(SET.outputs)
		this.$testCases.empty()
		for (var i in SET.in) {
			if ('A' in window)
				var $inout = window.A.render($('#testcase-template').html(), this.$testCases,'dom').attr({'data-index':i});
			else {
				var $inout = $('<div/>',{'class':'test-case','data-index':i})
				$inout.html($('#testcase-template').html())
				this.$testCases.append($inout)
			}
			var $ins = $inout.find('.test-inputs'),
				$outs = $inout.find('.test-outputs');
			for (var I in SET.in[i]) {
				if ('A' in window)
					var $in = window.A.render($('#testput-template').html(), $ins,'dom').attr({'data-index':I});
				else {
					var $in = $('<div/>',{'class':'test-xput','data-index':I})
					$in.html($('#testput-template').html())
					$ins.append($in);
				}
				$in.find('input').val(SET.in[i][I])
			}
			for (var O in SET.out[i]) {
				if ('A' in window)
					var $out = window.A.render($('#testput-template').html(),$outs,'dom').attr({'data-index':O});
				else {
					var $out = $('<div/>',{'class':'test-xput','data-index':O})
					$out.html($('#testput-template').html())
					$outs.append($out);
				}
				$out.find('input').val(SET.out[i][O])
			}
		}
		this.$testCases.find('input').change(function () {
			var i = $(this).closest('.test-case').attr('data-index'),
				j = $(this).closest('.test-xput').attr('data-index'),
				no = $(this).closest('.test-inputs,.test-outputs').is('.test-inputs') ? 'in' : 'out';
			SET[no][i][j] = parseInt($(this).val())
		})
		this.$testCases.find('.delete-test').dblclick(function () {
			var i = $(this).closest('.test-case').attr('data-index');
			SET.in.splice(i,1)
			SET.out.splice(i,1)
			_this.set_inputs()
		})
	}
	this.revise=function (decision) {
		//console.log(decision)
		var un_decision={}
		if (decision['kind']=='link') {
			//if add
			if (decision['action']=='add') {
				un_decision={'action':'remove','kind':'link','reff':this.NEURO.links.length}
				this.NEURO.links.push(decision['reff'])
			//else
			} else if (decision['action']=='remove') {
				//select random
				var reff=this.NEURO.links.splice(decision['reff'],1)
				un_decision={'action':'add','kind':'link','reff':reff[0]}
				//remove
			}
		//if neuron
		} else if (decision['kind']=='neuron') {
			//if add
			if (decision['action']=='add') {
				//add random (not O/R)
				un_decision={'action':'remove','kind':'neuron','reff':this.NEURO.neurons.length}
				if (decision['reff'].slice(0,2) == 'U:')
					this.NEURO.insert_part(decision['reff'].slice(2))();
				else this.NEURO.add_neuron(decision['reff'])();
				if ('value' in decision) this.NEURO.neurons[this.NEURO.neurons.length-1].O=decision['value']
			//if remove
			} else if (decision['action']=='remove') {
				//select neuron (not O/R)
				// this is the hard part, get links as well as neuron
				var reff=this.NEURO.neurons[decision['reff']].type;
				if (reff=='U')
					reff+=":"+this.NEURO.neurons[decision['reff']].part;
				un_decision={'action':'add','kind':'neuron','reff':this.NEURO.neurons[decision['reff']].type,'links':[]}
				if (this.NEURO.neurons[decision['reff']].type=='O' || this.NEURO.neurons[decision['reff']].type=='V') un_decision['value']=this.NEURO.neurons[decision['reff']].O
				for (var e in this.NEURO.neurons[decision['reff']].ends['in']) {
					var pl=this.NEURO.prev_link([parseInt(decision['reff']),'in',parseInt(e)])

					if (pl) {
						un_decision['links'].push([['in',e],pl])
					}
				}
				for (var e in this.NEURO.neurons[decision['reff']].ends['out']) {
					var nl=this.NEURO.next_link([parseInt(decision['reff']),'out',parseInt(e)])

					if (nl) {
						un_decision['links'].push([nl,['out',e]])
					}
				}
				this.NEURO.remove_neuron(decision['reff'])
			//if change
			} else if (decision['action']=='change') {
				//select neuron (V)
				un_decision={'action':'change','kind':'neuron','reff':decision['reff'],'value':this.NEURO.neurons[decision['reff']].O}
				this.NEURO.neurons[decision['reff']].O=decision['value']
			}
		}
		return un_decision
		//return [undo]
	}
	this.revisions=function() {
		if ((! SET.in.length) || (! SET.out.length))
			_this.stop();
		if (stopIt) return;
		//console.log('------------------revision')
		var exists=false;
		for (var sp in window.test_parts) {
			if (window.test_parts[sp].id==SET.id) {
				if (window.test_parts[sp].runs.length > testNo) {
					_this.load_test(testNo)()
					exists=true
					break;
				}
			}
		}
		if (exists) {
			_this.clean_up()
		} else {
			var dec=_this.decide()
			if (dec) {
				_this.revise(dec)
				_this.NEURO.draw()
				var tested=_this.tests()//_this.tests_set()
				//return tested
				if (tested=='done') {
					done=true
					_this.ret()
					_this.clean_up()
					return
				}
				//return
				//var tested=_this.tests()
				if (tested.count==SET.out.length*SET.outputs) {
					done=true
					_this.ret()
					_this.clean_up()
					return
				} else {
					done=false
				}
				if (!done && revs>300){
					_this.get_best()
					return
				}
				// if it passed any test
				if (tested && tested.count!=0) {
					// set inputs
					for (var I in SET['in'][0]) {
						_this.NEURO.neurons[_this.ends['in'][I]].O=SET['in'][0][I]
					}
					// run it
					revs+=3
					var ran=_this.NEURO.run(_this.revisions);
				} else {
					// revise again
					revs++
					_this.revisions()
				}
				master_count++
			} else {
				_this.revisions()
			}
		}
	}
	this.tests_set=function () {
		//make change to inner (revisions)
		//change input link/s
		var links={'in':[],'out':[]},
			tests=[];
		for (var i in _this.ends['in']) {
			var in_link=[parseInt(_this.ends['in'][i]),'out',0],
				in_links=[];
			//change output link/s
			for (var n in _this.NEURO.neurons) {
				var neuron=_this.NEURO.neurons[n]
				if (neuron.type!='O'&&neuron.type!='R') {
					for (var e in neuron.ends['in']) {
						in_links.push([[parseInt(n),'in',parseInt(e)],in_link])
					}
				}
			}
			links['in'].push(in_links)
		}
		for (var o in _this.ends['out']) {
			var out_link=[parseInt(_this.ends['out'][o]),'in',0],
				out_links=[];
			//change output link/s
			for (var n in _this.NEURO.neurons) {
				var neuron=_this.NEURO.neurons[n]
				if (neuron.type!='O'&&neuron.type!='R') {
					for (var e in neuron.ends['out']) {
						out_links.push([[parseInt(n),'out',parseInt(e)],out_link])
					}
				}
			}
			links['out'].push(out_links)
		}
		console.log('a')
		console.log(links)

		for (var l in links['in'][0]) {
			for (var L in links['out'][0]) {
				var to_link=[]
				for (var i in links['in']) {
					to_link.push(links['in'][i][l])
				}
				for (var o in links['out']) {
					to_link.push(links['out'][o][L])
				}
				var undos=[];
				for (var t in to_link) {
					var dec=_this.decide({'kind':'link','action':'add','reff':to_link[t]}),
					rev=_this.revise(dec);
					undos.push(rev)
				}
				var tested=_this.tests()
				if (tested.count==SET.out.length*SET.outputs) {
					done=true
					_this.ret()
					_this.clean_up()
					return 'done'
				} else {
					done=false
					for (var u=undos.length-1;u>=0;u--) {
						_this.revise(undos[u])
					}
				}
			}
		}
		if (tests.length) {
		tests.sort(function(a, b) {
			return b.count - a.count;
		})
		return tests[0]	//test each in set (tests())
		} else {
			return false
		}
	}
	this.get_link = function (out_links, in_links, R) {
		if (R === undefined) R = 0;
		if (R > 10 || out_links.length == 0 || in_links.length == 0) return {in:undefined,out:undefined};
		var Out = rand_i(out_links),
			In = rand_i(in_links);
		if (_this.NEURO.neurons[Out[0]].type == 'O' &&
			_this.NEURO.neurons[In[0]].type == 'R')
			return _this.get_link(out_links, in_links, R+1)
		return {in:In,out:Out}
	}
	this.get_best=function() {
		var rev_list=[]

		//Sort by count (highest)
		bests.sort(function(a, b) {
			return b.count - a.count;
		})
		//not bests 0, rand best
		var best_count=0,bobs=[]
		//get best run count
		for (var b in bests) {
			if (bests[b].count>best_count) {
				best_count=bests[b].count
			}
		}
		//get best of bests with best run count
		for (var b in bests) {
			if (bests[b].count==best_count && bests[b].runs<5) {
				bobs.push([b,bests[b]])
			}
		}
		var bi,
			best;
		//if bobs
		if (bobs.length){
			//sort by runs (fewest?)
			bobs.sort(function(a, b) {
				return a[1].runs - b[1].runs;
			})
			//then by count (biggest)
			bobs.sort(function(a, b) {
				return a[1].count - b[1].count;
			})
			//set best
			bi=bobs[0][0]
			best=bests[bobs[0][0]]

		} else if (best_count!=0){
			// sort by parts (fewest)
			bests.sort(function(a, b) {
				//console.log(a,b)
				if (!('state' in a)) return 1
				if (!('state' in b)) return -1
				return JSON.parse(a.state.neurons).length - JSON.parse(b.state.neurons).length;
			})
			// sort by runs
			bests.sort(function(a, b) {
				return a.runs - b.runs;
			})
			//set best
			bi=0
			best=bests[0]
		} else {
			// if all else fails, choose one at random
			var bi=rand_int(0,bests.length-1),
			best=bests[bi]
		}
		if (best.count!=0) {
			//reset to state
			console.log('snapback')
			console.log(bests[bi]['runs'])
			//console.log('b')
			console.log(bests[bi]['match_count'])
			bests[bi]['runs']++
			_this.NEURO.links=JSON.parse(best['state']['links'])
			_this.NEURO.neurons=_this.NEURO.load_neurons(JSON.parse(best['state']['neurons']))
			revs=0
			var bls=clone(_this.ends.out)
			for (var o in best.match_count) {
				//console.log(best.match_count[o])
				if (best.match_count[o]==SET.out.length) {
					var BL=best.back_links
						//console.log(BL)
					for (var o in _this.ends.out) {
						for (var b in BL) {
							if (BL[b].n==_this.ends.out[o]) {
								bls[o]=BL[b]
								break;
							}
						}
					}
				}
			}
			//now keep bl
			//100 revs keep bl
			//var i=0
			//while(i<1 ){//&& !done) {
			//console.log(bls)
				//i++
				//alert(0)
				//var dec=_this.decide({'not':[{'kind':'links','action':'remove','reff':[]},{'kind':'neuron','action':'remove','reff':[]}]})

			//}
			_this.run_timeout = window.setTimeout(_this.revisions,10)
		} else {
			_this.revisions()
		}
		return
	}
	this.tests=function() {
		//set neurons &links
		this.set_T()
		// --------ensure ends
		for (var o in this.ends['in']) {
			var o_link=[this.ends['in'][o],'out',0]
			if (! this.NEURO.next_link(o_link)) {
				//new link
				return false
			}
		}
		var bl=false

		for (var r in this.ends['out']) {
			this.NEURO.draw()
			var tb=this.NEURO.trace_back(this.ends['out'][r]),
				b_l=this.NEURO.back_linked(tb);
			//console.log(b_l)
			if (b_l){//&&b_l[0]!='out') {
				//bl=false
				if (! bl)bl=[]
				bl.push(tb)
			}
		}
		if (!bl) return false
		var count=0,
			match_count=[];
		for (var e in this.ends.out) {
			match_count.push(0)
		}
		master_match=[]
		//ensure match
		for (var i in SET['in']) {
			// set inputs
			for (var I in SET['in'][i]) {
				this.T_NEURO.neurons[this.ends['in'][I]].O=SET['in'][i][I]
			}
			var ran=this.T_NEURO.run();
			if (! ran) return false
			var match=0;
			for (var j in SET['out'][i]) {
				if (ran[j]==SET['out'][i][j]) {
					match++
					match_count[j]+=1
				}
			}
			count+=match
			//if (match==SET['out'][i].length) master_match++
			master_match.push(match)
		}
		var match_obj={'matches':clone(master_match),'count':count,
			'match_count':clone(match_count),
			'back_links':clone(bl),
		'state':{
			'neurons':JSON.stringify(_this.T_NEURO.save_neurons()),
			'links':JSON.stringify(_this.T_NEURO.links)

		}}
		if (match_obj.count>0) {
			for (var b in bests) {
				if (bests[b].count<match_obj.count || (bests[b].count==match_obj.count && (bests[b].count==0 || ('state' in bests[b] && JSON.parse(bests[b].state.neurons).length>JSON.parse(match_obj.state.neurons).length)))) {
					match_obj['runs']=0
					bests.splice(b,1,match_obj)
					break;
				}
			}
		}
		return match_obj

	}
	this.test=function() {
		//revise if false
		//else _this.ret()
	}
	this.ret=function() {
		console.log('f')
		console.log(this.NEURO.save_neurons())
		console.log(this.NEURO.links)
		if (SET.runs.length-1 < testNo) {
			SET.runs.push({
				beforeClean:{
					neurons:this.NEURO.save_neurons(),
					links:clone(this.NEURO.links)
				}
			});
		} else {
			SET.runs[testNo].afterClean={
				neurons:this.NEURO.save_neurons(),
				links:clone(this.NEURO.links)
			};
		}
		this.save()
		// this.NEURO.save()
		//this.NEURO.parts_list()
		this.NEURO.draw()
	}
	this.save=function() {
		var found=false
		for (var sp in window.test_parts) {
			if (SET.id==window.test_parts[sp]['id']) {
				window.test_parts[sp]=SET
				found=true
				break;
			}
		}
		if (! found) {
			SET.id = _this.next_part_id()
			window.test_parts.push(SET)
		}
		//console.log('c')
		localStorage.setItem('test_parts', JSON.stringify(window.test_parts));
		_this.parts_list()
		_this.tests_list()
	}
	this.undo=function (un_decision) {
		if (un_decision['kind']=='neuron' && un_decision['action']=='add') {
			var n=this.NEURO.neurons.length
			this.revise(un_decision)
			for (var l in un_decision['links']) {
				var link=un_decision['links'][l]
				for (var L in un_decision['links'][l]) {
					if (un_decision['links'][l][L].length==2) {
						link[L].unshift(parseInt(n))
					}
				}
				this.NEURO.links.push(link)
			}
		} else {
			this.revise(un_decision)
		}
	}
	this.un_clean=function (UNDO) {
		return function() {
			_this.undo(UNDO)
			_this.NEURO.draw()
			window.setTimeout(_this.clean,500)
		}
	}
	this.construct()
}
