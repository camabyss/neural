window.neuron_defs={
	'O':{
		'name':'open',
		'ends':{
			'out':['n']
		},
		'icon':['top green circle'],
		'button':'success'
	},
	'V':{
		'name':'value',
		'ends':{
			'out':['n']
		},
		'icon':['top blue circle'],
		'button':'primary'
	},
	'R':{
		'name':'return',
		'ends':{
			'in':['n']
		},
		'icon':['top red circle'],
		'button':'danger'
	},
	'N':{
		'name':'node',
		'ends':{
			'in':['n'],
			'out':['n']
		},
		'icon':[]
	},
	'W':{
		'name':'wait',
		'ends':{
			'in':['n','n','n','n'],
			'out':['n','n']//,'n','n']
		},
		'icon':[]
	},
	'H':{
		'name':'half',
		'ends':{
			'in':['n'],
			'out':['n','n']
		},
		'icon':[]
	},
	'D':{
		'name':'double',
		'ends':{
			'in':['n'],
			'out':['n','n']
		},
		'icon':['small line v']
	}
}

function drawPart(ctx,type,part,x,y) {
	// load custom draw
	var to_draw,ends={in:[],out:[]};
	if (type=='U') {
		to_draw=part.icon
		for (var n in part.neurons) {
			if (part.neurons[n].type=='O')
				ends.in.push(part.neurons[n])
			else if (part.neurons[n].type=='R')
				ends.out.push(part.neurons[n])
		}
		// to_draw=[
		//	 'medium line d1 top',
		//	 'medium line d1 top left',
		//	 'medium line d1 right',
		//	 'medium line d2 right',
		//	 // 'small line v top left',
		//	 // 'small line h top',
		//	 // 'small line v top',
		//	 // 'small line h right top',
		//	 // 'small line v right top',
		//	 // 'small line h left',
		//	 // 'small line v left',
		//	 // 'small line h',
		//	 // 'small line v',
		//	 // 'small line h right',
		//	 // 'small line v right',
		//	 // 'small line h bottom left',
		//	 // 'small line v bottom left',
		//	 // 'small line h bottom',
		//	 // 'small line v bottom',
		//	 // 'small line h right bottom',
		//	 // 'small line v right bottom',
		//	 ]
	} else {
		to_draw=window.neuron_defs[type].icon
		ends=window.neuron_defs[type].ends
	}
	// draw ends & lines
	for (var e in ends) {
		for (var E in ends[e]) {
			// value
				var val='n',
					s_val;
				//do size math
				if (val!='n' && (val+1) > 20) {
					s_val=20
				} else if (val!='n') {
					s_val=val+1
				} else {
					s_val=1
				}
			// /value
			// get position
				var p_x,p_y;
				var angle=(180/(ends[e].length+1))*(parseInt(E)+1)-90
					p_y=y+(20*Math.sin((angle*Math.PI)/180))
				if (e=='in') {
					p_x=x-(20*Math.cos((angle*Math.PI)/180))
				} else if (e=='out') {
					p_x=x+(20*Math.cos((angle*Math.PI)/180))
				}
			// / get position

			// draw line
				ctx.beginPath();
				ctx.moveTo(x,y)
				ctx.lineTo(p_x,p_y)
				// if value, highlight
				if (val!='n') {
					ctx.lineWidth=s_val
					ctx.strokeStyle = '#2C75FF';
				}
				else {
					ctx.lineWidth=s_val
					ctx.strokeStyle = 'black';
				}
				ctx.stroke();
			// /draw line

			// draw end
				ctx.beginPath();
				var rad
				// if selected, highlight
				if (ends[e][E].selected) {
					ctx.strokeStyle = 'black';
					ctx.fillStyle='yellow';
					ctx.lineWidth=1
					rad=3
				}
				// else if value, highligh
				else if (val!='n') {
					ctx.strokeStyle='#2C75FF'
					ctx.fillStyle='#2C75FF';
					ctx.lineWidth=s_val
					rad=0.1//(s_val/2)+1
				} else {
					ctx.strokeStyle='black'
					ctx.fillStyle='black';
					ctx.lineWidth=1
					rad=1//(s_val/2)+1
				}
				ctx.arc(p_x,p_y,rad,0*Math.PI,2*Math.PI);
				ctx.fill();
				ctx.stroke();
			// /draw end

			// draw values
				if (val!='n' && type!='N') {
					if (e=='out') {
						ctx.fillStyle = "black";
						ctx.fillText(val, x+8, y+6);
					} else
					if (e=='in') {
						// ctx.fillStyle = "black";
						// ctx.fillText(val, x-18, y+6);
					}
				}
			// /draw values
		}
	}
	var bootstrap_colours={
		blue:'#337ab7',
		red:'#d9534f',
		green:'#5cb85c',
		blueborder:'#2e6da4',
		redborder:'#d43f3a',
		greenborder:'#4cae4c'
	}
	// draw custom
	for (var td in to_draw) {
		if (to_draw[td]) {
			var draw_set=to_draw[td].split(' '),
				kind='circle',
				size=10,
				position_h='center',
				position_v='center',
				direction='h',
				color='black',
				stroke_color=color,
				text='';
			for (var ds in draw_set) {
				switch (draw_set[ds]) {
					case 'h':
					case 'v':
					case 'd1':
					case 'd2':
						direction=draw_set[ds]
						break
					case 'red':
					case 'green':
					case 'blue':
					case 'yellow':
						if (draw_set[ds] in bootstrap_colours) {
							color=bootstrap_colours[draw_set[ds]]
							stroke_color=bootstrap_colours[draw_set[ds]+'border']
						} else {
							color=draw_set[ds]
							stroke_color=color
						}
						break
					case 'medium':
						size=10
						break
					case 'big':
						size=20
						break
					case 'small':
						size=5
						break
					case 'top':
					case 'bottom':
						position_v=draw_set[ds]
						break
					case 'left':
					case 'right':
						position_h=draw_set[ds]
						break
					case 'center':
						position_h='center'
						position_v='center'
						break
					case 'line':
					case 'circle':
					case 'text':
						kind=draw_set[ds]
						break
					default:
						text=draw_set[ds]
				}
			}
			if (kind=='line' && running) {
				color='#2C75FF'
				ctx.lineWidth=2
			} else {
				ctx.lineWidth=1
			}
			if (kind=='line') {
				var p_x=[],p_y=[];
				if (direction=='h') {
					if (position_h=='left') {
						p_x=[(x-size)-20,(x+size)-20]
					} else if (position_h=='right') {
						p_x=[(x-size)+20,(x+size)+20]
					} else if (position_h=='center') {
						p_x=[x-size,x+size]
					}
					if (position_v=='top') {
						p_y=[y-20,y-20]
					} else if (position_v=='bottom') {
						p_y=[y+20,y+20]
					} else if (position_v=='center') {
						p_y=[y,y]
					}
				} else 
				if (direction=='v') {
					if (position_h=='left') {
						p_x=[x-10,x-10]
					} else if (position_h=='right') {
						p_x=[x+10,x+10]
					} else if (position_h=='center') {
						p_x=[x,x]
					}
					if (position_v=='top') {
						p_y=[(y-size)-10,(y+size)-10]
					} else if (position_v=='bottom') {
						p_y=[(y-size)+10,(y+size)+10]
					} else if (position_v=='center') {
						p_y=[y-size,y+size]
					}
				} 
				else 
				if (direction=='d1') {
					if (position_h=='left') {
						p_x=[(x-(size/2))-10,(x+(size/2))-10]
					} else if (position_h=='right') {
						p_x=[(x-(size/2))+10,(x+(size/2))+10]
					} else if (position_h=='center') {
						p_x=[x-(size/2),x+(size/2)]
					}
					if (position_v=='top') {
						p_y=[((y+10)-(size/2))-10,((y+10)+(size/2))-10]
					} else if (position_v=='bottom') {
						p_y=[((y+10)-(size/2))+10,((y+10)+(size/2))+10]
					} else if (position_v=='center') {
						p_y=[((y+10)-(size/2)),((y+10)+(size/2))]
					}
				} else 
				if (direction=='d2') {
					if (position_h=='left') {
						p_x=[(x-(size/2))-10,(x+(size/2))-10]
					} else if (position_h=='right') {
						p_x=[(x-(size/2))+10,(x+(size/2))+10]
					} else if (position_h=='center') {
						p_x=[x-(size/2),x+(size/2)]
					}
					if (position_v=='top') {
						p_y=[((y+10)+(size/2))-10,((y+10)-(size/2))-10]
					} else if (position_v=='bottom') {
						p_y=[((y+10)+(size/2))+10,((y+10)-(size/2))+10]
					} else if (position_v=='center') {
						p_y=[((y+10)+(size/2)),((y+10)-(size/2))]
					}
				}
				ctx.beginPath();
				ctx.moveTo(p_x[0],p_y[0]);
				ctx.lineTo(p_x[1],p_y[1]);
				ctx.strokeStyle = color;
				ctx.stroke();
			} else if (kind=='circle') {
				var x,y;
				if (position_v=='top') {
					y=y
				} else
				if (position_v=='bottom') {
					y=y+20
				} else if (position_v=='center') {
					y=y+10
				}
				if (position_h=='left') {
					x=x-20
				} else
				if (position_h=='right') {
					x=x+20
				} else
				if (position_h=='center') {
					x=x
				}

				ctx.beginPath();
				ctx.arc(x,y,size,0*Math.PI,2*Math.PI);
				ctx.strokeStyle = stroke_color;
				ctx.fillStyle=color;
				ctx.fill();
				ctx.stroke();
			}
			else if (kind=='text') {
				ctx.fillStyle = "black";
				var x_add=0
				if (text.length<2) x_add=5
				ctx.fillText(text, (x-9)+x_add, y+16);
			}
		}
	}

	// draw values
	if (type=='O' || type=='V') {
		ctx.fillStyle = "white";
		var x_add=0,text=''
		if (text.length<2) x_add=5
		ctx.fillText(text, (x-9)+x_add, y+6);
	}
	if (type=='R') {
		ctx.fillStyle = "white";
		var x_add=0,text=''
		if (text.length<2) x_add=5
		else if (text.length>2) x_add=-10
		ctx.fillText(text, (x-9)+x_add, y+6);
	}
	if (type=='W') {
		ctx.fillStyle = "black";
		ctx.fillText('0', x-3, y-5);
		ctx.fillText('1', x-3, y+17);
	}
	running=false
}
function Neuron(Type,part,runnable) {
	if (runnable === undefined)
		runnable = true;
	var _this=this
	this.type=Type
	this.running=false
	this.x=
	this.y=100
	this.run_ins=[]
	this.construct=function () {
		this.running=false
		this.ends={'in':[],'out':[]}
		if (this.type == 'U') {
			if (this.part===undefined) this.part=part
			else part=this.part
			this.NEURO=new Neuro(this.part)
			this.ends={'in':[],'out':[]}
			for (var n in this.NEURO.neurons) {
				var neuron=this.NEURO.neurons[n]
				if (neuron.type=='O') this.ends['in'].push({'value':'n','selected':0})
				else if (neuron.type=='R') this.ends['out'].push({'value':'n','selected':0})
			}
		} else {
			for (var e in window.neuron_defs[this.type].ends) {
				this.ends[e]=[]
				for (var E in window.neuron_defs[this.type].ends[e]) {
					var end=window.neuron_defs[this.type].ends[e][E]
					this.ends[e].push({'value':end,'selected':0})
				}
			}
		}
		for (var e in _this.ends) {
			for (var E in _this.ends[e]) {
				var x,y;
				var angle=(180/(_this.ends[e].length+1))*(parseInt(E)+1)-90
					y=_this.y+(20*Math.sin((angle*Math.PI)/180))
				if (e=='in') {
					_this.run_ins.push('n')
					x=_this.x-(20*Math.cos((angle*Math.PI)/180))
				} else if (e=='out') {
					x=_this.x+(20*Math.cos((angle*Math.PI)/180))
				}
				_this.ends[e][E]['x']=x
				_this.ends[e][E]['y']=y
			}
		}
	}
	this.run=function () {
		_this.running=true
		var run=false
		
		for (var i in _this.ends['in']) {
		   // _this.run_ins[i]='n'
			if (_this.ends['in'][i].value != 'n') run=true
		}
		if (_this.type=='O' || _this.type=='V') run=true 
		if (run) {
			switch (_this.type) {
				case 'U':
					//reset & run neuro
					var i=0
					for (var n in _this.NEURO.neurons) {
						if (_this.NEURO.neurons[n].type=='O') {
							_this.NEURO.neurons[n].O=_this.ends['in'][i].value
							i++
						}
					}
					var ran=_this.NEURO.run()
					for (var r in ran) {
						_this.ends['out'][r].value=ran[r]
					}
					break
				case 'W':
					// always 4, never 'n'
					_this.ends['out'][0].value='n'
					_this.ends['out'][1].value='n'
					//_this.ends['out'][2].value='n'
					//_this.ends['out'][3].value='n'
					if (_this.ends['in'][0].value!='n') this.A=_this.ends['in'][0].value
					if (_this.ends['in'][3].value!='n') this.B=_this.ends['in'][3].value
					if (_this.ends['in'][1].value!='n') this.A+=_this.ends['in'][1].value
				// 	if (_this.ends['in'][0].value!='n' || _this.ends['in'][1].value!='n'){
				// 		if (this.A>this.B) {
				// 			_this.ends['out'][2].value=this.A
				// 		} else {
				// 			_this.ends['out'][3].value=this.B
				// 		}
				// 	}
					if (_this.ends['in'][2].value==this.B) {
						_this.ends['out'][0].value=this.A
						this.A=0
					}
					else if (_this.ends['in'][2].value!='n') {
						_this.ends['out'][1].value=_this.ends['in'][2].value
					}
					break
				case 'O':
				case 'V':
					_this.ends['out'][0].value=this.O
					break
				case 'R':
					this.setR(_this.ends['in'][0].value)
					break
				case 'H':
					if (_this.ends['in'][0].value!=0){
					_this.ends['out'][0].value=Math.ceil(_this.ends['in'][0].value/2)
					_this.ends['out'][1].value=Math.floor(_this.ends['in'][0].value/2)
					} else {
						_this.ends['out'][0].value='n'
						_this.ends['out'][1].value='n'
					}
					break
				case 'D':
					_this.ends['out'][0].value=_this.ends['in'][0].value
					_this.ends['out'][1].value=_this.ends['in'][0].value
					break
			}
			for (var i in _this.ends['in']) {
				_this.run_ins[i]=_this.ends['in'][i].value
				_this.ends['in'][i].value='n'
			}
		} else {
			return
		}
		
		_this.ends['in']
	}
	this.draw=function (ctx) {
		
		// load custom draw
		var to_draw
		if (_this.type=='U') {
			to_draw=_this.NEURO.icon
			// to_draw=[
			//	 'medium line d1 top',
			//	 'medium line d1 top left',
			//	 'medium line d1 right',
			//	 'medium line d2 right',
			//	 // 'small line v top left',
			//	 // 'small line h top',
			//	 // 'small line v top',
			//	 // 'small line h right top',
			//	 // 'small line v right top',
			//	 // 'small line h left',
			//	 // 'small line v left',
			//	 // 'small line h',
			//	 // 'small line v',
			//	 // 'small line h right',
			//	 // 'small line v right',
			//	 // 'small line h bottom left',
			//	 // 'small line v bottom left',
			//	 // 'small line h bottom',
			//	 // 'small line v bottom',
			//	 // 'small line h right bottom',
			//	 // 'small line v right bottom',
			//	 ]
		} else {
			to_draw=window.neuron_defs[_this.type].icon
		}
		
		// draw ends & lines
		for (var e in _this.ends) {
			for (var E in _this.ends[e]) {
				// value
					var val=((e=='in') ? _this.run_ins[E] : _this.ends[e][E].value),
						s_val;
					//do size math
					if (val!='n' && (val+1) > 20) {
						s_val=20
					} else if (val!='n') {
						s_val=val+1
					} else {
						s_val=1
					}
				// /value
				// get position
					var x,y;
					var angle=(180/(_this.ends[e].length+1))*(parseInt(E)+1)-90
						y=_this.y+(20*Math.sin((angle*Math.PI)/180))
					if (e=='in') {
						x=_this.x-(20*Math.cos((angle*Math.PI)/180))
					} else if (e=='out') {
						x=_this.x+(20*Math.cos((angle*Math.PI)/180))
					}
					_this.ends[e][E]['x']=x
					_this.ends[e][E]['y']=y
				// / get position
				
				// draw line
					ctx.beginPath();
					ctx.moveTo(_this.x,_this.y)
					ctx.lineTo(x,y)
					// if value, highlight
					if (val!='n') {
						ctx.lineWidth=s_val
						ctx.strokeStyle = '#2C75FF';
					}
					else {
						ctx.lineWidth=s_val
						ctx.strokeStyle = 'black';
					}
					ctx.stroke();
				// /draw line
				
				// draw end
					ctx.beginPath();
					var rad
					// if selected, highlight
					if (_this.ends[e][E].selected) {
						ctx.strokeStyle = 'black';
						ctx.fillStyle='yellow';
						ctx.lineWidth=1
						rad=3
					}
					// else if value, highligh
					else if (val!='n') {
						ctx.strokeStyle='#2C75FF'
						ctx.fillStyle='#2C75FF';
						ctx.lineWidth=s_val
						rad=0.1//(s_val/2)+1
					} else {
						ctx.strokeStyle='black'
						ctx.fillStyle='black';
						ctx.lineWidth=1
						rad=1//(s_val/2)+1
					}
					ctx.arc(_this.ends[e][E].x,this.ends[e][E].y,rad,0*Math.PI,2*Math.PI);
					ctx.fill();
					ctx.stroke();
				// /draw end
				
				// draw values
					if (val!='n' && _this.type!='N') {
						if (e=='out') {
							ctx.fillStyle = "black";
							ctx.fillText(val, x+8, y+6);
						} else
						if (e=='in') {
							// ctx.fillStyle = "black";
							// ctx.fillText(val, x-18, y+6);
						}
					}
				// /draw values
			}
		}
		var bootstrap_colours={
			blue:'#337ab7',
			red:'#d9534f',
			green:'#5cb85c',
			blueborder:'#2e6da4',
			redborder:'#d43f3a',
			greenborder:'#4cae4c'
		}
		// draw custom
		for (var td in to_draw) {
			if (to_draw[td]) {
				var draw_set=to_draw[td].split(' '),
					kind='circle',
					size=10,
					position_h='center',
					position_v='center',
					direction='h',
					color='black',
					stroke_color=color,
					text='';
				for (var ds in draw_set) {
					switch (draw_set[ds]) {
						case 'h':
						case 'v':
						case 'd1':
						case 'd2':
							direction=draw_set[ds]
							break
						case 'red':
						case 'green':
						case 'blue':
						case 'yellow':
							if (draw_set[ds] in bootstrap_colours) {
								color=bootstrap_colours[draw_set[ds]]
								stroke_color=bootstrap_colours[draw_set[ds]+'border']
							} else {
								color=draw_set[ds]
								stroke_color=color
							}
							break
						case 'medium':
							size=10
							break
						case 'big':
							size=20
							break
						case 'small':
							size=5
							break
						case 'top':
						case 'bottom':
							position_v=draw_set[ds]
							break
						case 'left':
						case 'right':
							position_h=draw_set[ds]
							break
						case 'center':
							position_h='center'
							position_v='center'
							break
						case 'line':
						case 'circle':
						case 'text':
							kind=draw_set[ds]
							break
						default:
							text=draw_set[ds]
					}
				}
				if (kind=='line' && _this.running) {
					color='#2C75FF'
					ctx.lineWidth=2
				} else {
					ctx.lineWidth=1
				}
				if (kind=='line') {
					var x=[],y=[];
					if (direction=='h') {
						if (position_h=='left') {
							x=[(_this.x-size)-20,(_this.x+size)-20]
						} else if (position_h=='right') {
							x=[(_this.x-size)+20,(_this.x+size)+20]
						} else if (position_h=='center') {
							x=[_this.x-size,_this.x+size]
						}
						if (position_v=='top') {
							y=[_this.y-20,_this.y-20]
						} else if (position_v=='bottom') {
							y=[_this.y+20,_this.y+20]
						} else if (position_v=='center') {
							y=[_this.y,_this.y]
						}
					} else 
					if (direction=='v') {
						if (position_h=='left') {
							x=[_this.x-10,_this.x-10]
						} else if (position_h=='right') {
							x=[_this.x+10,_this.x+10]
						} else if (position_h=='center') {
							x=[_this.x,_this.x]
						}
						if (position_v=='top') {
							y=[(_this.y-size)-10,(_this.y+size)-10]
						} else if (position_v=='bottom') {
							y=[(_this.y-size)+10,(_this.y+size)+10]
						} else if (position_v=='center') {
							y=[_this.y-size,_this.y+size]
						}
					} 
					else 
					if (direction=='d1') {
						if (position_h=='left') {
							x=[(_this.x-(size/2))-10,(_this.x+(size/2))-10]
						} else if (position_h=='right') {
							x=[(_this.x-(size/2))+10,(_this.x+(size/2))+10]
						} else if (position_h=='center') {
							x=[_this.x-(size/2),_this.x+(size/2)]
						}
						if (position_v=='top') {
							y=[((_this.y+10)-(size/2))-10,((_this.y+10)+(size/2))-10]
						} else if (position_v=='bottom') {
							y=[((_this.y+10)-(size/2))+10,((_this.y+10)+(size/2))+10]
						} else if (position_v=='center') {
							y=[((_this.y+10)-(size/2)),((_this.y+10)+(size/2))]
						}
					} else 
					if (direction=='d2') {
						if (position_h=='left') {
							x=[(_this.x-(size/2))-10,(_this.x+(size/2))-10]
						} else if (position_h=='right') {
							x=[(_this.x-(size/2))+10,(_this.x+(size/2))+10]
						} else if (position_h=='center') {
							x=[_this.x-(size/2),_this.x+(size/2)]
						}
						if (position_v=='top') {
							y=[((_this.y+10)+(size/2))-10,((_this.y+10)-(size/2))-10]
						} else if (position_v=='bottom') {
							y=[((_this.y+10)+(size/2))+10,((_this.y+10)-(size/2))+10]
						} else if (position_v=='center') {
							y=[((_this.y+10)+(size/2)),((_this.y+10)-(size/2))]
						}
					}
					ctx.beginPath();
					ctx.moveTo(x[0],y[0]);
					ctx.lineTo(x[1],y[1]);
					ctx.strokeStyle = color;
					ctx.stroke();
				} else if (kind=='circle') {
					var x,y;
					if (position_v=='top') {
						y=_this.y
					} else
					if (position_v=='bottom') {
						y=_this.y+20
					} else if (position_v=='center') {
						y=_this.y+10
					}
					if (position_h=='left') {
						x=_this.x-20
					} else
					if (position_h=='right') {
						x=_this.x+20
					} else
					if (position_h=='center') {
						x=_this.x
					}
					
					ctx.beginPath();
					ctx.arc(x,y,size,0*Math.PI,2*Math.PI);
					ctx.strokeStyle = stroke_color;
					ctx.fillStyle=color;
					ctx.fill();
					ctx.stroke();
				}
				else if (kind=='text') {
					ctx.fillStyle = "black";
					var x_add=0
					if (text.length<2) x_add=5
					ctx.fillText(text, (_this.x-9)+x_add, _this.y+16);
				}
			}
		}
		
		// draw values
		if (_this.type=='O' || _this.type=='V') {
			ctx.fillStyle = "white";
			var x_add=0,text=''+_this.O
			if (text.length<2) x_add=5
			ctx.fillText(text, (_this.x-9)+x_add, _this.y+6);
		}
		if (_this.type=='R') {
			ctx.fillStyle = "white";
			var x_add=0,text=''+_this.R
			if (text.length<2) x_add=5
			else if (text.length>2) x_add=-10
			ctx.fillText(text, (_this.x-9)+x_add, _this.y+6);
		}
		if (_this.type=='W') {
			ctx.fillStyle = "black";
			ctx.fillText(_this.A, _this.x-3, _this.y-5);
			ctx.fillText(_this.B, _this.x-3, _this.y+17);
		}
		_this.running=false
	}
	this.copy=function () {
		for (var t in Type) {
			if (t=='x'||t=='y'||t=='O'||t=='part'||t=='type')
			assign(_this,t,Type[t])
		}
		Type=_this.type
	}
	this.setR=function (v) {this.R=v}
	this.save=function () {
		//save type, part & x,y
		var obj={'type':this.type,'x':this.x,'y':this.y}
		if (this.type=='U') obj['part']=this.part
		if (this.type=='O' || this.type=='V') obj['O']=this.O
		return obj
	}
	if (typeof Type == 'object') {
		this.copy()
	}
	if ((this.type=='O' || this.type=='V') && this.O==undefined) {
		this.O=10
	} else if (this.type=='R') {
		this.R='n'
	}
	if (this.type=='W') {
		this.A=0
		this.B=1
	}
	this.construct()
}