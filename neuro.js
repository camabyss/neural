function Neuro(part,graphic,e) {
	var _this=this
	this.part_name='new neuro'
	this.icon=[]
	this.neurons=[]
	this.links=[]
	this.current_links=[]
	this.run_delay=100
	this.run_limit=500
	this.to_run={'current':[],'next':[],'in':[],'in_next':[]}
	if (part===undefined) part='new'
	if (graphic===undefined) graphic=false
	this.part=part

	this.clear = function (except) {
		if (except===undefined)
			except=["O","R"];
		else if (typeof except == 'string')
			except=[except];
		else if (except == true)
			except=[];
		for (var i=_this.neurons.length-1;i>=0;i--) {
			var typ = _this.neurons[i].type;
			if (except.indexOf(typ) == -1) {
				_this.remove_neuron(i)
			}
		}
		_this.draw()
	}
	this.afterBuild=function (find) {
		if (find === undefined) find=false;
		this.$name=$('#neuroN')
		this.$icon=$('#neuroI')
		this.$delay=$('#neuroD')
		this.$limit=$('#neuroL')
		this.canvas=this.$canvas[0]
		this.ctx = this.canvas.getContext('2d');
		this.ctx.font = "bold 16px Arial";
		this.ctx.lineCap="round";
		for (var nd in window.neuron_defs) {
			var $neuron=false,$neuron_img=false,
				n_def=window.neuron_defs[nd];
			if (find) {
				$neuron=$('.neuro_neuron[neuron='+nd+']');
				$neuron_img=$('canvas#neuron_img'+nd);
			}
			if (! $neuron || ! $neuron.length) {

				if ('A' in window) {
					$neuron=window.A.render($('#neuro-neuron-template').html(),_this.$neurons,'dom')
					$neuron_img = $neuron.find('.part_img').attr({width:50,height:50,id:'neuron_img'+nd})
					$neuron.find('span').html(n_def.name);
					$neuron.attr({'neuron':nd})
				} else {
					$neuron=$('<div/>',{
						'class':'neuro_neuron btn btn-default'/*+('button' in n_def ? n_def['button'] : 'default')*/,
						'neuron':nd}).html(n_def.name);
					$neuron_img=$('<canvas/>',{'class':'part_img',id:'neuron_img'+nd}).attr({width:50,height:50});
					this.$neurons.append($neuron.append($neuron_img))
				}
			}

			drawPart($neuron_img[0].getContext("2d"),nd,n_def,25,25)
			$neuron.unbind('click').click(this.add_neuron(nd))
		}
		this.parts_list()
		this.event()
		this.draw()
	}
	this.find=function () {
		this.$canvas=$('canvas.neuro_canvas')
		this.$actions=$('.neuro_actions')
		this.$navcontain=$('.neuro_navcontain')
		this.$neurons=$('.neuro_neurons')
			this.$values=$('.neuro_values')
			this.$fileinfo=$('.neuro_fileinfo')
			this.$run=$('.neuro_run')
			this.$save=$('.neuro_save')
			this.$clear=$('.neuro_clear')
			this.$new=$('.new_part')
		this.$neuros=$('.neuro_neuros')
		this.afterBuild(true)
	}
	this.build=function () {
		e.empty()
		this.$canvas=$('<canvas width=500 height=400 />',{'class':'neuro_canvas'})
		this.$actions=$('<nav/>',{'class':'neuro_actions'})
		this.$navcontain=$('<div/>',{'class':'neuro_navcontain'})
		this.$neurons=$('<nav/>',{'class':'neuro_neurons'})
			this.$values=$('<div/>',{'class':'neuro_values'})
			this.$fileinfo=$('<div/>',{'class':'neuro_fileinfo'})
			this.$run=$('<div/>',{'class':'neuro_run btn btn-primary'}).html("<i class='glyphicon glyphicon-play' ></i> run")
			this.$save=$('<div/>',{'class':'neuro_save btn btn-success'}).html("<i class='glyphicon glyphicon-floppy-disk'></i> save")
			this.$new=$('<div/>',{'class':'new_part btn btn-warning'}).html("<i class='glyphicon glyphicon-plus'></i> New Net")
		this.$neuros=$('<nav/>',{'class':'neuro_neuros'})
		e
			.append(this.$canvas)
			.append(this.$navcontain
				.append(this.$neurons
					.append(this.$run))
				.append(this.$actions
					.append(this.$new)
					.append(this.$save)
					.append(this.$fileinfo)
					.append(this.$values))
				.append(this.$neuros))
		this.afterBuild()
	}
	this.parts_list=function () {
		_this.$neuros.empty()
		for(var sp in window.save_parts) {
			if ('A' in window) {

				var $part=window.A.render($('#neuro-part-template').html(),_this.$neuros,'dom'),
						$part_img=$part.find('canvas.part_img').attr({width:50,height:50,id:'part_img'+sp}),
						$part_name=$part.find('.part_name'),
						$part_delete=$part.find('.part_delete'),
						$part_insert=$part.find('.part_insert');
				$part_name.find('span').html(window.save_parts[sp]['name']);
			} else {
				var $part=$('<div/>',{'class':'part btn-group btn-group-sm',title:'edit'}),
						$part_img=$('<canvas/>',{'class':'part_img',id:'part_img'+sp}).attr({width:50,height:50}),
						$part_name=$('<div/>',{'class':'part_name btn btn-default'}).html(window.save_parts[sp]['name']),
						$part_delete=$('<div/>',{'class':'part_delete btn btn-danger',title:'delete'}).html("<i class='glyphicon glyphicon-remove'></i>"),
						$part_insert=$('<div/>',{'class':'part_insert btn btn-primary',title:'insert'}).html("<i class='glyphicon glyphicon-plus'></i>");
				_this.$neuros
					.append($part
						.append($part_name
							.append($part_img))
						.append($part_insert)
						.append($part_delete))
			}

			drawPart($part_img[0].getContext("2d"),'U',window.save_parts[sp],25,25)

			$part_name.click(_this.load_part(window.save_parts[sp].id))
			$part_delete.dblclick(_this.delete_part(sp))
			$part_insert.click(_this.insert_part(window.save_parts[sp].id))
		}
	}
	this.delete_part=function (sp) {
		return function () {
			window.save_parts.splice(sp,1)
			localStorage.setItem('parts', JSON.stringify(window.save_parts));
			_this.parts_list()
		}
	}
	this.load_part=function(sp) {
		return function () {

			_this.part=sp
			_this.construct()
			// _this.$fileinfo.html($('#fileinfo-template').html())
			if (graphic && window.NEURAL) {
				window.NEURAL.load_neuro()
				_this.set_inputs()
			}
		}
	}
	this.set_inputs = function () {

		var $n=_this.$name,
			$i=_this.$icon,
			$d=_this.$delay,
			$l=_this.$limit,
			n_f=function () {
				_this.part_name=$(this).val()
			},
			i_f = function () {
				_this.icon=$(this).val().split(',')
			},
			d_f = function () {
				_this.run_delay=parseInt($(this).val())
			},
			l_f = function () {
				_this.run_limit=parseInt($(this).val())
			};
		$n.val(_this.part_name)
		$n.unbind('keypress').keypress(function (e) {
			var k = e.keyCode || e.which;
			if (k == 13) {
				n_f.call(this)
			}
		});
		$d.val(_this.run_delay)
		$d.unbind('keypress').keypress(function (e) {
			var k = e.keyCode || e.which;
			if (k == 13) {
				d_f.call(this)
			}
		});
		$l.val(_this.run_limit)
		$l.unbind('keypress').keypress(function (e) {
			var k = e.keyCode || e.which;
			if (k == 13) {
				l_f.call(this)
			}
		});
		$i.val(_this.icon.join(','))
		$i.unbind('keypress').keypress(function (e) {
			var k = e.keyCode || e.which;
			if (k == 13) {
				i_f.call(this)
				// return false
			}
		});
		$n.unbind('change').change(n_f)
		$i.unbind('change').change(i_f)
		$d.unbind('change').change(d_f)
		$l.unbind('change').change(l_f)
	}
	this.stop=function() {
		window.clearTimeout(_this.time_out)
	}
	this.toggleRun = function () {
		if (_this.$run.hasClass('running')) {
			_this.$run.removeClass('running btn-danger').addClass('btn-primary');
			_this.$run.html("<i class='glyphicon glyphicon-play' ></i> run");
			_this.stop()
		} else {
			_this.$run.addClass('running btn-danger').removeClass('btn-primary');
			_this.$run.html("<i class='glyphicon glyphicon-stop' ></i> stop");
			_this.run(_this.toggleRun)
		}
	}
	this.run=function (after) {
		_this.to_run={'current':[],'next':[],'in':[],'in_next':[]}
		//reset
		for (var n in _this.neurons) {
			var neuron=_this.neurons[n]
			if (neuron.type=='R') neuron.R='n'
			if (neuron.type=='W') {
				neuron.A=0
				neuron.B=1
			}
			for (var e in neuron.ends) {
				for (var E in neuron.ends[e]) {
					if (neuron.type=='U') {
						neuron.ends[e][E].value='n'
					} else {
						neuron.ends[e][E].value=window.neuron_defs[neuron.type].ends[e][E]
					}
				}
			}
		}
		if (graphic) _this.draw()
		// get opens
		for (var n in _this.neurons) {
			if (_this.neurons[n].type=='O' || _this.neurons[n].type=='V') {
				_this.to_run['next'].push(n)
			}
		}
		_this.runs=0
		if (graphic) {
			_this.time_out=window.setTimeout(_this.run_next(after),_this.run_delay)
		} else {
			// while steps
			while (_this.to_run['next'].length && _this.runs<_this.run_limit) {
				_this.RUN()
			}
			if (_this.runs>=_this.run_limit) return false
			// return returns
			var ret=[]
			for (var n in _this.neurons) {
				var neuron=_this.neurons[n]
				if (neuron.type=='R') ret.push(neuron.R)
			}
			return ret
		}
	}
	this.RUN=function () {
		//console.log('-------------running')
		_this.runs++
		_this.to_run['current']=_this.to_run['next']
		_this.to_run['next']=[]
		_this.to_run['in']=_this.to_run['in_next']
		_this.to_run['in_next']=[]
		_this.current_links=[]
		for (var n in _this.neurons) {
			for (var e in _this.neurons[n].ends.out) {
				_this.neurons[n].ends.out[e].value='n'
			}
		}
		while (_this.to_run['current'].length) {
			var tr=_this.to_run['current'].shift(),
				nl=_this.next_links(tr),
				cl=_this.cur_links(tr);
			for (var iv in _this.to_run['in']) {
				var TR=_this.to_run['in'][iv]
				if (TR[0]==tr) {
					_this.neurons[tr].ends['in'][TR[1]].value=TR[2]
				}
			}
			_this.neurons[tr].run()
			//get next and append if not already
			for (var n in nl) {
				if (nl[n][1]) {
					var NL=nl[n][1]
					if (_this.neurons[tr].ends.out[nl[n][0]].value!='n' && NL!=undefined){
						_this.current_links.push([cl[n],_this.neurons[tr].ends.out[nl[n][0]].value])
						if (_this.to_run['next'].indexOf(NL[0])==-1) {
							_this.to_run['next'].push(NL[0])
						}
						_this.to_run['in_next'].push([NL[0],NL[2],_this.neurons[tr].ends.out[n].value])
					}
				}
			}
		}
		if (graphic) _this.draw()
		for (var n in _this.neurons) {
			for (var e in _this.neurons[n].ends['in']) {
				_this.neurons[n].run_ins[e]='n'
			}
		}
	}
	this.run_next=function (after) {
		return function () {
			_this.RUN()
			//
			if (_this.to_run['next'].length && _this.runs<_this.run_limit) _this.time_out=window.setTimeout(_this.run_next(after),_this.run_delay)
			// else returns
			else {
				var ret=[]
				for (var n in _this.neurons) {
					var neuron=_this.neurons[n]
					if (neuron.type=='R') ret.push(neuron.R)
				}
				if (isFunction(after)) {
					if (_this.runs>=_this.run_limit) after(false)
					else after(ret)
				}
			}
		}
	}
	this.draw=function () {
		if (! graphic) return;
		//reset
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		//links
		for (var l in _this.links) {
			_this.ctx.beginPath();
			for (var L in _this.links[l]) {
				var link=_this.links[l][L],
					end=_this.neurons[link[0]].ends[link[1]][link[2]];
				if (L==0) _this.ctx.moveTo(end.x,end.y);
				else _this.ctx.lineTo(end.x,end.y);
			}
			_this.ctx.strokeStyle = 'black';
			_this.ctx.lineWidth=1

			_this.ctx.stroke();
		}
		//neurons
		for (var n in this.neurons) {
			this.neurons[n].draw(this.ctx)
		}
		// live links
		for (var l in _this.links) {

			for (var cl in _this.current_links) {
				if (_this.current_links[cl][0]==l) {
					_this.ctx.beginPath();
					for (var L in _this.links[l]) {
						var link=_this.links[l][L],
							end=_this.neurons[link[0]].ends[link[1]][link[2]];
						if (L==0) _this.ctx.moveTo(end.x,end.y);
						else _this.ctx.lineTo(end.x,end.y);
					}
					_this.ctx.strokeStyle = '#2C75FF';
					var val=_this.current_links[cl][1]
					_this.ctx.lineWidth=val+1
					if (val>20) _this.ctx.lineWidth=20
					_this.ctx.stroke();
					break
				}
			}
		}
	}
	this.save_neurons=function(){
		var ret=[]
		for (var n in this.neurons) {
			ret.push(this.neurons[n].save())
		}
		return ret
	}
	this.save=function() {
		var found=false
		for (var sp in window.save_parts) {
			if (_this.part==window.save_parts[sp]['id']) {
				window.save_parts[sp]={
					'name':_this.part_name,
					'neurons':_this.save_neurons(),
					'links':clone(_this.links),
					'icon':_this.icon,
					'id':_this.part
				}
				found=true
			}
		}
		if (! found) {
			_this.part = _this.next_part_id()
			window.save_parts.push({
				'name':_this.part_name,
				'neurons':_this.save_neurons(),
				'links':clone(_this.links),
				'icon':_this.icon,
				'id':_this.part
			})
		}
		//console.log('c')
		localStorage.setItem('parts', JSON.stringify(window.save_parts));
		if (graphic) _this.parts_list()
	}
	this.construct=function () {
		if (_this.part!='new') {
			this.neurons=[]
			this.links=[]
			this.to_run={'current':[],'next':[],'in':[],'in_next':[]}
			this.icon=[]
			this.current_links=[]
			var PART=_this.get_part()
			this.part_name=PART['name']
			this.neurons=this.load_neurons()
			this.links=clone(PART['links'])
			this.to_run={'current':[],'next':[],'in':[],'in_next':[]}
			this.icon=PART['icon']
			this.current_links=[]
			if (this.icon===undefined) this.icon=[]
		} else {
			this.part_name='new neuro'
			this.neurons=[]
			this.links=[]
			this.to_run={'current':[],'next':[],'in':[],'in_next':[]}
			this.icon=[]
			this.current_links=[]
			this.run_delay=100
			this.run_limit=200
		}
		if (graphic) {
			if (e===undefined)
				this.find();
			else this.build();
		}
	}
	this.event=function () {
		//and some vars to track the dragged item
		_this.dragIdx = -1;
		_this.dragOffsetX
		_this.dragOffsetY
		_this.canvas.removeEventListener("mousedown",_this.mousedown);
		_this.canvas.addEventListener("mousedown",_this.mousedown);
		_this.canvas.removeEventListener("dblclick",_this.dblclick);
		_this.canvas.addEventListener("dblclick",_this.dblclick);
		_this.$run.unbind('click').click(_this.toggleRun)
		_this.$new.unbind('click').click(_this.load_part('new'))
		_this.$save.unbind('click').click(_this.save)
		_this.$clear.unbind('click').click(function(){_this.clear()})
	}
	this.unload = function () {

		this.stop();
		this.clear();
	}
	this.next_links=function(n) {
		var neuron=_this.neurons[n],
			ret=[]
		if ('out' in neuron.ends) {
			for (var E in neuron.ends.out) {
				ret.push([parseInt(E),_this.next_link([n,'out',E])])
			}
		}
		return ret
	}
	this.cur_links=function(n) {
		var neuron=_this.neurons[n],
			ret=[]
		if ('out' in neuron.ends) {
			for (var E in neuron.ends.out) {
				ret.push(_this.next_link([n,'out',E],true))
			}
		}
		return ret
	}
	this.next_link=function(link,Index) {
		if (Index===undefined) Index=false
		for (var l in _this.links) {
			var match=0
			if (_this.links[l][1]===undefined||_this.links[l][0]==undefined) match=0
			else {
				for (var L in _this.links[l][1]) {
					if (_this.links[l][1][L]==link[L]) match++
				}
				if (match==3) {
					var l_link=_this.links[l][0]
					if (_this.neurons[l_link[0]].type=='N') {
						this.neurons[l_link[0]].running=true
						this.current_links.push([l,1])
						this.current_links.push([_this.next_link([l_link[0],'out',0],true),1])
						return _this.next_link([l_link[0],'out',0],Index)
					} else {
						if (Index) return l
						else return l_link
					}
				}
			}
		}
		return false
	}
	this.prev_link=function(link,Index) {
		if (Index===undefined) Index=false
		for (var l in _this.links) {
			var match=0
			if (_this.links[l][1]===undefined||_this.links[l][0]==undefined) match=0
			else {
				for (var L in _this.links[l][0]) {
					if (_this.links[l][0][L]==link[L]) match++
				}
				if (match==3) {
					if (_this.neurons[_this.links[l][1][0]].type=='N') {
						return _this.prev_link([_this.links[l][1][0],'in',0],Index)
					} else {
						if (Index) return l
						else return _this.links[l][1]
					}
				}
			}
		}
		return false
	}
	this.load_neurons=function (SET) {

		var ret=[],mods,PART=_this.get_part();
		if (SET===undefined) mods=PART['neurons'];
		else mods=SET
		for(var m in mods) {
			ret.push(new Neuron(mods[m]))
		}
		return ret
	}
	this.add_neuron=function (nd) {
		return function () {
			_this.neurons.push(new Neuron(nd))
			if (typeof nd!='object') {
				var prev_neuron=_this.neurons[_this.neurons.length-1],
				pos={'x':100,'y':100},
					hit=true,
					count=0;

			   if(prev_neuron.type=='O') {
				   pos.x=20
				   pos.y=((400/4)*(_this.get_type('O').length-1))+50
			   } else if (prev_neuron.type=='R') {
				   pos.x=480
				   pos.y=((400/4)*(_this.get_type('R').length-1))+50
			   } else {
				   while (hit && count < 40) {
					   var HIT=_this.obj_hit(pos)
					   if (HIT) {
						   pos.x=rand_int(50,450)
						   pos.y=rand_int(50,350)
					   } else {
						   hit=false
						   break;
					   }
					   count++
				   }
				}
				prev_neuron.x=pos.x
				prev_neuron.y=pos.y
			}
			if (graphic) _this.draw()
		}
	}
	this.getMousePos=function(evt) {
		var rect = this.canvas.getBoundingClientRect();
		return {
		  x: evt.clientX - rect.left,
		  y: evt.clientY - rect.top
		};
	  }
	this.find_ends=function() {
		var ret=[]
		for (var n in _this.neurons) {
			var neuron=_this.neurons[n]
			for (var e in neuron.ends){
				for (var E in neuron.ends[e]) {
					if (neuron.ends[e][E].selected) ret.push([parseInt(n),e,parseInt(E)])
				}
			}
		}
		return ret
	}
	// Clicking
	this.dblclick=function(e) {
		var mouse=_this.getMousePos(e)
		//...calc coords into mouseX, mouseY
		for(var i=_this.neurons.length-1; i>=0; i--){ //loop in reverse draw order
			var neuron=_this.neurons[i],
				dx = mouse.x - neuron.x,
				dy = mouse.y - neuron.y;
			//check end hit
			for (var e in neuron.ends){
				for (var E in neuron.ends[e]) {
					var end=neuron.ends[e][E],
						bdx = mouse.x - end.x,
						bdy = mouse.y - end.y;
					if (Math.sqrt((bdx*bdx) + (bdy*bdy)) < 5) {
						//remove links
						_this.remove_links([i,e,E])
						_this.draw()
						return
					}
				}
			}
			//if no end hit, check total hit
			if (Math.sqrt((dx*dx) + (dy*dy)) < 20) {
				_this.remove_neuron(i)
				_this.draw()
				return;
			}
		}
	}
	this.remove_neuron=function(i){
		for (var e in _this.neurons[i].ends){
			for (var E in _this.neurons[i].ends[e]) {
				_this.remove_links([i,e,E])
			}
		}
		_this.neurons.splice(i,1)
		for (var l in _this.links) {
			for (var L in _this.links[l]) {

				if (_this.links[l][L]!=undefined && _this.links[l][L][0]>i) {
					_this.links[l][L][0]--
				}
			}
		}
	}
	this.remove_links=function(link){
		var rem=[];
		for (var l in _this.links) {
			for (var L in _this.links[l]) {
				var match=0
				for (var ll in _this.links[l][L]){
					if(_this.links[l][L][ll]==link[ll]) match++
				}
				if (match==3) rem.push(parseInt(l))
			}
		}
		for (var l=_this.links.length-1;l>=0;l--) {
			if (rem.indexOf(l)!=-1) {
				_this.links.splice(l,1)
			}
		}
	}
	this.mousedown=function(e) {
		var mouse=_this.getMousePos(e)
		for(var i=_this.neurons.length-1; i>=0; i--){
			var neuron=_this.neurons[i],
				dx = mouse.x - neuron.x,
				dy = mouse.y - neuron.y;
			//check end hit
			for (var e in neuron.ends){
				for (var E in neuron.ends[e]) {
					var end=neuron.ends[e][E],
						bdx = mouse.x - end.x,
						bdy = mouse.y - end.y;
					if (Math.sqrt((bdx*bdx) + (bdy*bdy)) < 5) {
						if (end.selected) {
							neuron.ends[e][E].selected=0
						} else {
							var fe=_this.find_ends()
							for (var f in fe) {
								if (fe[f][1]=='out' && e=='in') {
									_this.links.push([[parseInt(i),e,parseInt(E)],fe[f]])
									neuron.ends[e][E].selected=0
									_this.neurons[fe[f][0]].ends[fe[f][1]][fe[f][2]].selected=0
									_this.draw()
									return
								} else if (fe[f][1]=='in' && e=='out') {
									_this.links.push([fe[f],[parseInt(i),e,parseInt(E)]])
									neuron.ends[e][E].selected=0
									_this.neurons[fe[f][0]].ends[fe[f][1]][fe[f][2]].selected=0
									_this.draw()
									return
								}
							}
							neuron.ends[e][E].selected=1
						}
						_this.draw()
						return
					}
				}
			}
			//if no end hit, check total hit
			_this.$values.hide()
			if (Math.sqrt((dx*dx) + (dy*dy)) < 20) {
				// editing vals?
				if (_this.neurons[i].type=='O' || _this.neurons[i].type=='V') {
					// _this.$values.html($('#inputvalue-template').html())
					_this.$values.show()
					var $o=$('#neuroO'),
						o_f=function () {
							_this.neurons[i].O=parseInt($(this).val())
							_this.draw()
						};
					$o.val(_this.neurons[i].O)
					$o.unbind('keypress').keypress(function (e) {
						var k = e.keyCode || e.which;
						if (k == 13) {
							o_f.call(this)
						}
					});
					$o.unbind('change').change(o_f)
				}
				//we've hit an item
				_this.dragIdx = i; //store the item being dragged
				_this.dragOffsetX = dx; //store offsets so item doesn't 'jump'
				_this.dragOffsetY = dy;
				_this.canvas.addEventListener("mousemove",_this.mousemove); //start dragging
				_this.canvas.addEventListener("mouseup",_this.mouseup);
				return;
			}
		}
		// if no hit, neuro options
		//$('#run').click()
	}
	// Dragging
	this.mousemove=function(e) {
		var mouse=_this.getMousePos(e)
		_this.neurons[_this.dragIdx].x = mouse.x + _this.dragOffsetX; //drag your item
		_this.neurons[_this.dragIdx].y = mouse.y + _this.dragOffsetY;
		_this.draw ()
	}
	this.mouseup=function(e) {
		_this.dragIdx = -1; //reset for next mousedown
		_this.canvas.removeEventListener("mousemove",_this.mousemove)
		_this.canvas.removeEventListener("mouseup",_this.mouseup)//.... //remove the move/up events when done
	}
	this.insert_part=function (sp) {
		return function () {
			_this.neurons.push(new Neuron('U',sp))
			_this.draw()
		}
	}
	this.destroy=function() {
		_this.neurons=[]
		_this.links=[]
		_this.to_run={'current':[],'next':[],'in':[],'in_next':[]}
		_this.canvas.removeEventListener("mousedown",_this.mousedown);
		_this.$run.unbind('click')
		_this.$save.unbind('click')
		e.empty()
	}
	this.get_type=function(Type) {
		var ret=[]
		for (var n in this.neurons) {
			if (this.neurons[n].type==Type) ret.push(n)
		}
		return ret
	}
	this.trace_back=function(n,prev_l) {
		if (prev_l===undefined) prev_l=[n]
		else prev_l.push(n)
		if(this.neurons[n].type=='O'){
			return 'end'
		} else {
			var links=false,
				ret={'n':n,'l':[]};
			for (var e in this.neurons[n].ends['in']) {
				var pl=this.prev_link([parseInt(n),'in',parseInt(e)])
				if (pl && prev_l.indexOf(pl[0])==-1) {
					var Pl=this.trace_back(pl[0],clone(prev_l));
					if (Pl=='end') {
						ret.l.push([pl[0],this.prev_link([parseInt(n),'in',parseInt(e)],true)])
					}  else if (Pl && Pl) {
						ret.l.push([Pl,this.prev_link([parseInt(n),'in',parseInt(e)],true)])
					}
				}
			}
			if (ret.l.length!=0) return ret
			else return false
		}
	}
	this.back_linked=function (traced) {
		//var rs=this.get_type('R')
		//console.log(clone(traced))
		if (traced=='out') return true
		if (traced==false) return false
		else {
			var bl=false
			for (var l in traced.l) {
				if (typeof traced.l[l][0]=='number') return true
				else if (traced.l[l][0]) {
					var BL=this.back_linked(traced.l[l][0])
					if (BL) bl=true
				}
			}
			return bl
		}
	}
	this.obj_hit=function(pos) {
		for(var i=_this.neurons.length-1; i>=0; i--){
			var neuron=_this.neurons[i],
				dx = pos.x - neuron.x,
				dy = pos.y - neuron.y;
			//if no end hit, check total hit
			if (Math.sqrt((dx*dx) + (dy*dy)) < 50) {
				return true
			}
		}
		return false
	}
	this.get_part=function() {
		for (var p in window.save_parts) {
			if (window.save_parts[p]['id']==_this.part)
				return window.save_parts[p];
		}
	}
	this.next_part_id=function () {
		var found=false,
			id=0;
		while (! found) {
			var found_cur=false;
			for (var p in window.save_parts) {
				if (window.save_parts[p]['id']==id) {
					found_cur=true
					break;
				}
			}
			if (!found_cur) return id;
			else id++
		}
	}
	this.construct()

}
