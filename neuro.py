import math
import thread
nets={
    'minus_one':{
        'a':{
            'type':'H'
        },
        'b':{
            'type':'W',
            'c':1
        },
        'c':{
            'type':'W'
        },
        'links':['o1:aL1','aR1:bL2','aR2:cL1','bR1:cL2','bR2:aL1','cR1:r']
    },
    'to_one':{
        'a':{
            'type':'H'
        },
        'b':{
            'type':'W',
            'c':1
        },
        'links':['o1:aL1','aR2:bL2','bR1:r','bR2:aL1']
    }
}
minusMany={
    'a':{
        'type':'H'
    },
    'b':{
        'type':'W'
    },
    'c':{
        'type':'D'
    },
    'd':{
        'type':'W',
        'b':0
    },
    'e':{
        'type':'D'
    },
    'f':{
        'type':'W'
    },
    'links':['o1:aL1','aR1:bL2','aR2:dL1','o2:bL1','bR1:minus_one:eL1','bR2:cL1','cR1:fL1','cR2:aL1','dR1:aL1','eR1:bL1','eR2:to_one:fL2','fR1:dL2']
}
toOne={
    'a':{
        'type':'H'
    },
    'b':{
        'type':'W',
        'c':1
    },
    'links':['o1:aL1','aR2:bL2','bR1:r','bR2:aL1']
}
def Half (n):
    return [int(math.ceil(n/2.0)),int(math.floor(n/2.0))]
def Join (n,m):
    return n+m
def Double (n):
    return [n,n]
class Wait:
    def __init__(self,options):
        self.C=0
        self.B=1
        if 'b' in options:
            self.B=options['b']
        if 'c' in options:
            self.C=options['c']
    def accumulate(self,n):
        self.C+=n
    def test(self,n):
        if n==self.B:
            c=self.C
            self.C=0
            return [c,0]
        else:
            return [0,n]

###
class net:
    def __init__(self,objs):
        self.objs=objs
        self.get_wait()
    def get_wait (self):
        for c in self.objs:
            if c!='links' and self.objs[c]['type']=='W':
                self.objs[c]['e']=Wait(self.objs[c])
    def findLink(self,t='o'):
        for l in self.objs['links']:
            L=l.split(':')
            if L[0]==t:
                return L[1:]
            # elif L[1]==t:
            #     return L[0]
        return [False]
    def run(self,Input):
        self.A='working'
        for I in range(0,len(Input)):
            o=self.findLink('o'+str(I+1))
            thread.start_new_thread( self.path, (o,Input[I]) )
        while self.A=='working':
            pass
        return self.A
    def path(self,op,n):
        STR=''
        o=op[-1]
        if len(op)>1 and o and n:
            STR+=str(op[0])+'\n'
            nnet=net(nets[op[0]]).run([n])
            thread.start_new_thread( self.path, ([o], nnet) )
        elif o and n:
            STR+=('-')+'\n'
            STR+=(o)+'\n'
            STR+=str(n)+'\n'
            R=[]
            if o=='r':
                self.A=n
                STR+=('RRRRRR'+str(n))+'\n'
                return
            STR+=str(self.objs[o[0]]['type'])+'\n'
            if self.objs[o[0]]['type']=='H':
                R=Half(n)
            elif self.objs[o[0]]['type']=='D':
                R=Double(n)
            elif self.objs[o[0]]['type']=='W':
                if o[1:]=='L1':
                    self.objs[o[0]]['e'].accumulate(n)
                elif o[1:]=='L2':
                    R=self.objs[o[0]]['e'].test(n)
            STR+=str(R)+'\n'
            for r in range(0,len(R)):
                fl=self.findLink(str(o[0])+'R'+str(r+1))
                STR+=(str(o[0])+'R'+str(r+1)+' -> '+str(fl[-1]))+'\n'
                thread.start_new_thread( self.path, (fl, R[r]) )
        print(STR)
mO=net(nets['minus_one'])
print mO.run([11])
# tO=net(toOne)
# print tO.run([1019248364])
# mM=net(minusMany)
# print mM.run([6,22])