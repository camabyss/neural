//http://stackoverflow.com/questions/13719593/javascript-how-to-set-object-property-given-its-string-name
function assign(obj, prop, value) {
	if (typeof prop === "string")
		prop = prop.split(".");

	if (prop.length > 1) {
		var e = prop.shift();
		assign(obj[e] =
				 Object.prototype.toString.call(obj[e]) === "[object Object]"
				 ? obj[e]
				 : {},
				prop,
				value);
	} else
		obj[prop[0]] = value;
}
function rand_int(min,max) {
	return Math.floor((Math.random()*((max-min)+1))+min)
}
function rand_i(LIST) {
	return LIST[rand_int(0,LIST.length-1)]
}

function clone(obj) {
    //console.log('CC')
    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        var copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        var copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}
function d_count(dict){
	var count=0
	for (var d in dict) {
		count++
	}
	return count
}

function isFunction(functionToCheck) {
 var getType = {};
 return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}