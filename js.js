//http://www.w3schools.com/js/js_cookies.asp
function setCookie(cname,cvalue,exdays)
{
var d = new Date();
d.setTime(d.getTime()+(exdays*24*60*60*1000));
var expires = "expires="+d.toGMTString();
document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname)
{
var name = cname + "=";
var ca = document.cookie.split(';');
for(var i=0; i<ca.length; i++) 
  {
  var c = ca[i].trim();
  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
}
return "";
}

function checkCookie()
{
var user=getCookie("username");
if (user!="")
  {
  alert("Welcome again " + user);
  }
else 
  {
  user = prompt("Please enter your name:","");
  if (user!="" && user!=null)
    {
    setCookie("username",user,365);
    }
  }
}
//http://stackoverflow.com/questions/13719593/javascript-how-to-set-object-property-given-its-string-name
function assign(obj, prop, value) {
    if (typeof prop === "string")
        prop = prop.split(".");

    if (prop.length > 1) {
        var e = prop.shift();
        assign(obj[e] =
                 Object.prototype.toString.call(obj[e]) === "[object Object]"
                 ? obj[e]
                 : {},
               prop,
               value);
    } else
        obj[prop[0]] = value;
}
window.modules=[]
window.links=[]
window.toRun=[]
window.part_name='my neuro'
var canvas=$('#main_canvas')[0],
    ctx = canvas.getContext('2d');
function Module(Type,sp) {
    var _this=this;
    this.build=function () {
        this.type=Type
        this.x=50
        this.y=100
        this.r=20
        this.ends={'r':[],'l':[]}
        this.ret=[]
        this.sp=sp
        switch(Type) {
            case 'U':
                this.in_defs=[]
                for (var m in window.save_parts[this.sp]['modules']) {
                    var mod=window.save_parts[this.sp]['modules'][m]
                    if (mod.type=='O') {
                        this.ends.l.push(0)
                        this.in_defs.push(mod.O)
                    }else if (mod.type=='R') {
                        this.ends.r.push(0)
                    }
                }
                _this.nn=new neuro(_this.sp)
                break
            case 'N':
                this.ends.l.push(0)
                this.ends.r.push(0)
                break
            case 'O':
                this.ends.r.push(0)
                this.O=10
                break
            case 'R':
                this.ends.l.push(0)
                this.R=0
                break
            case 'H':
            case 'D':
                this.ends.l.push(0)
                this.ends.r.push(0)
                this.ends.r.push(0)
                break;
            case 'J':
                this.ends.l.push(0)
                this.ends.l.push(0)
                this.ends.r.push(0)
                break;
            case 'W':
                this.ends.l.push(0)
                this.ends.l.push(0)
                this.ends.r.push(0)
                this.ends.r.push(0)
                this.ends['t']=[0]
                this.ends['b']=[0]
                this.Ci=0
                this.C=_this.Ci
                this.B=1
                break;
        }
        this.draw()
    }
    this.run=function (Input) {
        _this.ret=[];
        //console.log(Type)
        if (Type=='U') {
            i=0
            for (var m in _this.nn.modules) {
                if(_this.nn.modules[m].type=='O') {
                    if(i<Input.length) {
                        console.log('_______')
                        console.log(_this.nn.modules[m].O)
                        console.log(Input[i])
                    _this.nn.modules[m].O=Input[i]
                        console.log(_this.nn.modules[m].O)
                    i++
                    }
                }
            }
            _this.nn.run()
            for (var m in _this.nn.modules) {
                if(_this.nn.modules[m].type=='R') {
                    _this.ret.push(_this.nn.modules[m].R)
                }
            }
            
        } else
        if (Type=='O') {
            _this.ret=[_this.O]
        }
        else if (Type=='R') {
            //console.log(Input[0])
            _this.R=Input[0]
            // if ($('input[name=OP]').length) {
            //     var $vallist=$('input[name=OP]').val().split(', ')
            //     $vallist.push(Input[0])
            //     $('input[name=OP]').val($vallist.join(', '))
            //     return []
            // }
        }
        else if (Type=='H' && Input[0]) {
            _this.ret=[Math.ceil(Input[0]/2),Math.floor(Input[0]/2)]
        }
        else if (Type=='J') {
            _this.ret=[Input[0]+Input[1]]
        }
        else if (Type=='D') {
            _this.ret=[Input[0],Input[0]]
        }
        else if (Type=='W') {
            for (var i=0;i<Input.length;i++) {
                //console.log(i)
                //console.log('I:'+i)
                //console.log(Input[i] )
                if (Input[i] || (i==1 && Input[i]==_this.B)) {
                    if (i==1) {
                        if (Input[i]==_this.B) {
                            var c=_this.C
                            _this.C=0
                            _this.ret=[c,0]
                            break
                        } else {
                            if (Input.length==1) Input.push(0)
                            _this.ret=[0,Input[i]]
                            break
                        }
                    } else if (i==0) {
                        //console.log(_this.C)
                        if (Input[i]) _this.C=parseInt(_this.C)+parseInt(Input[i])
                        //console.log(_this.C)
                        
                        // console.log('C:'+_this.C)
                    } else if (i==2) {
                        // console.log('ci')
                        // console.log(Input[i])
                        _this.Ci=Input[i]
                        _this.C=_this.Ci
                    } else if (i==3) {
                        _this.B=Input[i]
                    }
                }
            }
        }
        return _this.ret
    }
    this.draw=function () {
        ctx.beginPath();
        if (Type=='U') {
            ctx.arc(_this.x,_this.y,20,0*Math.PI,2*Math.PI);
            ctx.fillStyle = "blue";
            ctx.font = "bold 16px Arial";
            for (var Id in _this.in_defs) {
                ctx.fillText(_this.in_defs[Id], _this.x-45, _this.y+(20*Id));
            }
            
        } else
        if (Type=='N') {
            ctx.moveTo(_this.x-20,_this.y);
            ctx.lineTo(_this.x+20,_this.y);
        } else
        if (Type=='O') {
            ctx.moveTo(_this.x,_this.y);
            ctx.lineTo(_this.x+20,_this.y);
            ctx.stroke();
            ctx.beginPath();
            ctx.arc(_this.x,_this.y,10,0*Math.PI,2*Math.PI);
            ctx.fillStyle="yellow";
            ctx.fill();
            ctx.fillStyle = "blue";
            ctx.font = "bold 16px Arial";
            ctx.fillText(_this.O, _this.x-30, _this.y);
        } else if (Type=='R') {
            ctx.moveTo(_this.x,_this.y);
            ctx.lineTo(_this.x-20,_this.y);
            ctx.stroke();
            ctx.beginPath();
            ctx.arc(_this.x,_this.y,10,0*Math.PI,2*Math.PI);
            ctx.fillStyle="red";
            ctx.fill();
            ctx.fillStyle = "blue";
            ctx.font = "bold 16px Arial";
            ctx.fillText(_this.R, _this.x+15, _this.y);
        } else if (Type=='H') {
            ctx.moveTo(_this.x,_this.y);
            ctx.lineTo(_this.x-20,_this.y);
            ctx.moveTo(_this.x,_this.y);
            ctx.lineTo(_this.x+20,_this.y);
            ctx.moveTo(_this.x,_this.y);
            ctx.lineTo(_this.x+20,_this.y+20);
        } else if (Type=='D') {
            ctx.moveTo(_this.x,_this.y);
            ctx.lineTo(_this.x-20,_this.y);
            ctx.moveTo(_this.x,_this.y);
            ctx.lineTo(_this.x+20,_this.y);
            ctx.moveTo(_this.x,_this.y);
            ctx.lineTo(_this.x+20,_this.y+20);
            ctx.moveTo(_this.x,_this.y-5);
            ctx.lineTo(_this.x,_this.y+5);
        } else if (Type=='J') {
            ctx.moveTo(_this.x,_this.y);
            ctx.lineTo(_this.x-20,_this.y);
            ctx.moveTo(_this.x,_this.y);
            ctx.lineTo(_this.x+20,_this.y);
            ctx.moveTo(_this.x,_this.y);
            ctx.lineTo(_this.x-20,_this.y+20);
        } else if (Type=='W') {
            ctx.moveTo(_this.x,_this.y);
            ctx.lineTo(_this.x-20,_this.y);
            ctx.moveTo(_this.x,_this.y);
            ctx.lineTo(_this.x+20,_this.y);
            ctx.moveTo(_this.x,_this.y+20);
            ctx.lineTo(_this.x-20,_this.y+20);
            ctx.moveTo(_this.x,_this.y+20);
            ctx.lineTo(_this.x+20,_this.y+20);
            ctx.moveTo(_this.x,_this.y-5);
            ctx.lineTo(_this.x,_this.y+25);
            ctx.fillStyle = "blue";
            ctx.font = "bold 16px Arial";
            ctx.fillText(_this.Ci, _this.x, _this.y-20);
            ctx.fillText(_this.C, _this.x-20, _this.y-20);
            ctx.fillText(_this.B, _this.x, _this.y+50);
        }
        ctx.stroke();
        for (var s in this.ends){
            for (var end in this.ends[s]) {
                var x,y;//=_this.y+(end*20);
                if (s=='r') {
                    x=_this.x+20
                    y=_this.y+(end*20)
                    if (Type != 'N'){
                    if (_this.ret.length>end) {
                        ctx.fillStyle = "blue";
                        ctx.font = "bold 16px Arial";
                        ctx.fillText(_this.ret[end], _this.x+30, _this.y+(20*end)+5);
                    } else {
                        ctx.fillStyle = "blue";
                        ctx.font = "bold 16px Arial";
                        ctx.fillText('0', _this.x+30, _this.y+(20*end)+5);
                    }
                    }
                } else if (s=='l') {
                    x=_this.x-20
                    y=_this.y+(end*20)
                }else if (s=='t') {
                    y=_this.y-10
                    x=_this.x+(end*20)
                }else if (s=='b') {
                    y=_this.y+30
                    x=_this.x+(end*20)
                }
                ctx.beginPath();
                ctx.arc(x,y,5,0*Math.PI,2*Math.PI);
                ctx.stroke();
                if (this.ends[s][end]) {
                    ctx.fillStyle="green";
                } else {
                    ctx.fillStyle="blue";
                }
                ctx.fill();
            }
        }
    }
    this.copy=function () {
        for (var t in Type) {
            _this[t]=Type[t]
            assign(_this,t,Type[t])
        }
        sp=this.sp
        if (_this.type=='U') _this.nn=new neuro(_this.sp)
        Type=_this.type
        _this.ret=[]
    }
    if (typeof Type == 'object') {
        this.copy()
    } else {
        this.build()
    }
}
$('.module').click(function () {
    window.modules.push(new Module($(this).attr('id')))
})
//start with only the mousedown event attached
canvas.addEventListener("mousedown",mousedown);
canvas.addEventListener("dblclick",dblclick);

//and some vars to track the dragged item
var dragIdx = -1;
var dragOffsetX, dragOffsetY;
function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
  }
function dblclick(e) {
    var mouse=getMousePos(canvas,e)
    //...calc coords into mouseX, mouseY
    for(var i=window.modules.length-1; i>=0; i--){ //loop in reverse draw order
        
        var mod=window.modules[i],
            dx = mouse.x - mod.x,
            dy = mouse.y - mod.y;
        for (var s in mod.ends){
            for (var end in mod.ends[s]) {
                var x,y=mod.y+(end*20);
                if (s=='r') {
                    x=mod.x+20
                } else if (s=='l') {
                    x=mod.x-20
                }
                var bdx = mouse.x - x,
                    bdy = mouse.y - y;
                if (Math.sqrt((bdx*bdx) + (bdy*bdy)) < 5) {
                    removeLinks([parseInt(i),s,parseInt(end)])
                    redraw()
                    return
                }
            }
        }
        if (Math.sqrt((dx*dx) + (dy*dy)) < window.modules[i].r) {  
            // $('#vals').empty()  
            // if (window.modules[i].type=='W') {
            //     var $c=$('<input/>',{'name':'C'}),
            //         $b=$('<input/>',{'name':'B'});
            //     $c.val(window.modules[i].C)
            //     $c.keypress(function (e) {
            //         var k = e.keyCode || e.which;
            //         if (k == 13) {
            //             window.modules[i].C=$(this).val()
            //             redraw()
            //         }
            //     });
            //     $b.val(window.modules[i].B)
            //     $b.keypress(function (e) {
            //         var k = e.keyCode || e.which;
            //         if (k == 13) {
            //             window.modules[i].B=$(this).val()
            //             redraw()
                        
            //         }
            //     });
            //     $('#vals').append($c).append($b)
            // }else
            // if (window.modules[i].type=='O') {
            //     var $o=$('<input/>',{'name':'C'});
            //     $o.val(window.modules[i].O)
            //     $o.keypress(function (e) {
            //         var k = e.keyCode || e.which;
            //         if (k == 13) {
            //             window.modules[i].O=$(this).val()
            //             redraw()
            //         }
            //     });
            //     $('#vals').append($o)
            // }
            // //we've hit an item
            // dragIdx = i; //store the item being dragged
            // dragOffsetX = dx; //store offsets so item doesn't 'jump'
            // dragOffsetY = dy;
            // canvas.addEventListener("mousemove",mousemove); //start dragging
            // canvas.addEventListener("mouseup",mouseup);
            // return;
        }
    }
}
function mousedown(e) {
    var mouse=getMousePos(canvas,e)
    //...calc coords into mouseX, mouseY
    for(var i=window.modules.length-1; i>=0; i--){ //loop in reverse draw order
        
        var mod=window.modules[i],
            dx = mouse.x - mod.x,
            dy = mouse.y - mod.y;
        for (var s in mod.ends){
            for (var end in mod.ends[s]) {
                var x,y;
                if (s=='r') {
                    y=mod.y+(end*20)
                    x=mod.x+20
                } else if (s=='l') {
                    y=mod.y+(end*20)
                    x=mod.x-20
                } else if (s=='t') {
                    y=mod.y-10
                    x=mod.x+(end*20)
                }else if (s=='b') {
                    y=mod.y+30
                    x=mod.x+(end*20)
                }
                var bdx = mouse.x - x,
                    bdy = mouse.y - y;
                if (Math.sqrt((bdx*bdx) + (bdy*bdy)) < 5) {
                    if (mod.ends[s][end]) {
                        mod.ends[s][end]=0
                    } else {
                        var fe=find_ends()
                        for (var f in fe) {
                            if (fe[f][1]=='r' && s=='l') {
                                window.links.push([[parseInt(i),s,parseInt(end)],fe[f]])
                                mod.ends[s][end]=0
                                window.modules[fe[f][0]].ends[fe[f][1]][fe[f][2]]=0
                                redraw()
                                return
                            } else if (fe[f][1]=='l' && s=='r') {
                                window.links.push([fe[f],[parseInt(i),s,parseInt(end)]])
                                mod.ends[s][end]=0
                                window.modules[fe[f][0]].ends[fe[f][1]][fe[f][2]]=0
                                redraw()
                                return
                            } else if (fe[f][1]=='r' && (s=='t' || s=='b')) {
                                window.links.push([[parseInt(i),s,parseInt(end)],fe[f]])
                                mod.ends[s][end]=0
                                window.modules[fe[f][0]].ends[fe[f][1]][fe[f][2]]=0
                                redraw()
                                return
                            } else if (s=='r' && (fe[f][1]=='t' || fe[f][1]=='b')) {
                                window.links.push([fe[f],[parseInt(i),s,parseInt(end)]])
                                mod.ends[s][end]=0
                                window.modules[fe[f][0]].ends[fe[f][1]][fe[f][2]]=0
                                redraw()
                                return
                            }
                        }
                        mod.ends[s][end]=1
                    }
                    redraw()
                    return
                }
            }
        }
        if (Math.sqrt((dx*dx) + (dy*dy)) < window.modules[i].r) {  
            $('#vals').empty()  
            if (window.modules[i].type=='W') {
                var $c=$('<input/>',{'name':'C'}),
                    $b=$('<input/>',{'name':'B'});
                $c.val(window.modules[i].C)
                $c.keypress(function (e) {
                    var k = e.keyCode || e.which;
                    if (k == 13) {
                        window.modules[i].C=$(this).val()
                        redraw()
                    }
                });
                $b.val(window.modules[i].B)
                $b.keypress(function (e) {
                    var k = e.keyCode || e.which;
                    if (k == 13) {
                        window.modules[i].B=$(this).val()
                        redraw()
                        
                    }
                });
                $('#vals').append($c).append($b)
            }else
            if (window.modules[i].type=='O') {
                var $o=$('<input/>',{'name':'O'});
                $o.val(window.modules[i].O)
                $o.keypress(function (e) {
                    var k = e.keyCode || e.which;
                    if (k == 13) {
                        window.modules[i].O=parseInt($(this).val())
                        redraw()
                    }
                });
                $('#vals').append($o)
            }
            //we've hit an item
            dragIdx = i; //store the item being dragged
            dragOffsetX = dx; //store offsets so item doesn't 'jump'
            dragOffsetY = dy;
            canvas.addEventListener("mousemove",mousemove); //start dragging
            canvas.addEventListener("mouseup",mouseup);
            return;
        }
    }
    $('#vals').empty()  
    var $n=$('<input/>',{'name':'N'}),
        $op=$('<input/>',{'name':'OP'}),
        $save=$('<div/>',{'class':'save_part'}).html('save');
    $n.val(window.part_name)
    $n.keypress(function (e) {
        var k = e.keyCode || e.which;
        if (k == 13) {
            window.part_name=$(this).val()
        }
    });
    $save.click(function () {
        var found=false
        for (var sp in window.save_parts) {
            if (window.part_name==window.save_parts[sp]['name']) {
                window.save_parts[sp]={
                    'name':window.part_name,
                    'modules':window.modules,
                    'links':window.links
                }
                found=true
            }
        }
        if (! found) {
            window.save_parts.push({
                'name':window.part_name,
                'modules':window.modules,
                'links':window.links
            })
        }
        
        parts_list()
        localStorage.setItem('parts', JSON.stringify(window.save_parts));
        //setCookie('parts',JSON.stringify(window.save_parts),20000)
    })
    $('#vals').append($n).append($op).append($save)
    //$('#run').click()
}
function mousemove(e) {
    var mouse=getMousePos(canvas,e)
     window.modules[dragIdx].x = mouse.x + dragOffsetX; //drag your item
     window.modules[dragIdx].y = mouse.y + dragOffsetY;
    redraw ()
}

function mouseup(e) {
     dragIdx = -1; //reset for next mousedown
     canvas.removeEventListener("mousemove",mousemove)
     canvas.removeEventListener("mouseup",mouseup)//.... //remove the move/up events when done
}
function redraw () {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    for (var l in window.links) {
        ctx.beginPath();
        for (var L in window.links[l]) {
            var mod=window.modules[window.links[l][L][0]],s=window.links[l][L][1],x,y;
            if (s=='r') {
                y=mod.y+(window.links[l][L][2]*20)
                x=mod.x+20
            } else if (s=='l') {
                y=mod.y+(window.links[l][L][2]*20)
                x=mod.x-20
            } else if (s=='t') {
                y=mod.y-10
                x=mod.x
            }else if (s=='b') {
                y=mod.y+30
                x=mod.x
            }
            if (L) ctx.lineTo(x,y)
            else ctx.moveTo(x,y)
        }
        ctx.stroke();
    }
    for (var m in window.modules) {
        window.modules[m].draw()
    }
}
function find_ends() {
    var ret=[]
    for (var m in window.modules) {
        var mod=window.modules[m]
        for (var s in mod.ends){
            for (var end in mod.ends[s]) {
                if (mod.ends[s][end]) ret.push([parseInt(m),s,parseInt(end)])
            }
        }
    }
    return ret
}

$('#run').click(function () {
    window.toRun=[]
   for (var m in window.modules) {
       if (window.modules[m].type=='O') {
           window.toRun.push([m,[],''])
       }
   }
   //console.log()
   run_functs(JSON.parse(JSON.stringify(window.toRun)))()
})
function run_functs (trs) {
    //console.log(JSON.stringify(trs))
    return function () {
        while (trs.length) {
            var tr=trs.shift()
            window.toRun.shift()
            RUN(tr[2],tr[0],tr[1])
        }
        if (window.toRun.length) {
        setTimeout(run_functs(JSON.parse(JSON.stringify(window.toRun))),1000)
        }
    }
}
function RUN(parth,m,Input) {
    if (parth.length>100) return
    //console.log(parth+window.modules[m].type)
    //console.log('id:'+m)
    //console.log(Input)
    var out=window.modules[m].run(Input)
    redraw()
    //console.log(out)
    for (var o in out) {
        var nextlink=getLink([parseInt(m),'r',parseInt(o)]),
            nextInput=[];
        ////console.log(nextlink)
        if (nextlink) {
            var nl=nextlink[2];
            if (nextlink[1]=='t')nl=2
            else if (nextlink[1]=='b')nl=3
            if (nl>0) {
                for (var i=0;i<nl;i++) {
                    nextInput.push(0)
                }
            }
            //console.log('!!!')
            //console.log(nextlink)
            //console.log(nextInput)
            nextInput.push(out[o])
            window.toRun.push([nextlink[0],nextInput,parth+window.modules[m].type])
        }
    }
}
function getLink(link) {
    for (var l in window.links) {
        for (var L in window.links[l]) {
            var match=0,
                r;
            for (var j in window.links[l][L]) {
                if (window.links[l][L][j]==link[j]) match++
            }
            if (L) r=0
            else r=1
            if (match==3) {
                if (window.modules[window.links[l][r][0]].type=='N') {
                    return getLink([window.links[l][r][0],'r',0])//window.links[l][r]
                } else {
                    return window.links[l][r]
                }
            } 
        }
    }
    return false
}
function removeLinks(link) {
    var match=0;
    for (var l in window.links) {
        for (var L in window.links[l]) {
            match=0;
            var r;
            for (var j in window.links[l][L]) {
                if (window.links[l][L][j]==link[j]) match++
            }
            if (match==3) {
                window.links.splice(l,1)
                break
            }
        }//return window.links[l][r]
        if (match==3) break
    }
    if (match) removeLinks(link)
}
function parts_list() {
    $('#parts').empty()
    var $reset=$('<div/>',{'class':'reset_part'}).html('reset')
    $reset.click(function () {
        window.modules=[]
        window.links=[]
        window.toRun=[]
        window.part_name='my neuro'
        redraw()
    })
    $('#parts').append($reset)
    for(var sp in window.save_parts) {
        //console.log(window.save_parts[sp]['modules'])
        var $part=$('<div/>',{'class':'part'}),
                $part_name=$('<div/>',{'class':'part_name'}).html(window.save_parts[sp]['name']),
                $part_insert=$('<div/>',{'class':'part_insert'}).html('i');
        $('#parts').append($part.append($part_name).append($part_insert))
        $part_name.click(load_part(sp))
        $part_insert.click(insert_part(sp))
    }
}
function insert_part(sp) {
    return function () {
        window.modules.push(new Module('U',sp))
        redraw()
    }
}
function load_part(sp) {
    return function () {
        var SP=window.save_parts[sp]
        window.part_name=SP['name']
        window.modules=load_modules(SP['modules'])
        //console.log(window.modules)
        window.links=SP['links']
        window.toRun=[]
        redraw()
        }
}
function load_modules(mods) {
    var ret=[]
    for(var m in mods) {
        ret.push(new Module(mods[m]))
    }
    return ret
}
function neuro(part,graphic) {
    if (part===undefined) part='new'
    if (graphic===undefined) graphic=false
    var _this=this
    this.build=function () {
        if(part=='new') {
            this.modules=[]
            this.links=[]
            this.toRun=[]
        } else {
            console.log('-------inner')
            console.log(window.save_parts[part]['name'])
            this.modules=load_modules(window.save_parts[part]['modules'])
            this.links=window.save_parts[part]['links']
            this.toRun=[]
        }
    }
    this.run=function () {
        this.toRun=[]
        for (var m in this.modules) {
            if (this.modules[m].type=='O') {
                console.log('!!!!!!!!')
                console.log(this.modules[m].O)
                this.toRun.push([m,[],''])
            }
        }
       //console.log()
       _this.run_functs(JSON.parse(JSON.stringify(this.toRun)))()
    }
    this.run_functs=function (trs) {
        //console.log(JSON.stringify(trs))
        return function () {
            var delay
            if (graphic) delay=1000
            else delay=10
                while (trs.length) {
                    var tr=trs.shift()
                    _this.toRun.shift()
                    _this.RUN(tr[2],tr[0],tr[1])
                }
                if (_this.toRun.length) {
                    setTimeout(_this.run_functs(JSON.parse(JSON.stringify(_this.toRun))),delay)
                }
        }
    }
    this.RUN=function(parth,m,Input) {
        if (parth.length>100) return
        //console.log(parth+_this.modules[m].type)
        //console.log('id:'+m)
        //console.log(Input)
        var out=_this.modules[m].run(Input)
        if (graphic) _this.redraw()
        //console.log(out)
        for (var o in out) {
            var nextlink=_this.getLink([parseInt(m),'r',parseInt(o)]),
                nextInput=[];
            ////console.log(nextlink)
            if (nextlink) {
                var nl=nextlink[2];
                if (nextlink[1]=='t')nl=2
                else if (nextlink[1]=='b')nl=3
                if (nl>0) {
                    for (var i=0;i<nl;i++) {
                        nextInput.push(0)
                    }
                }
                //console.log('!!!')
                //console.log(nextlink)
                //console.log(nextInput)
                nextInput.push(out[o])
                _this.toRun.push([nextlink[0],nextInput,parth+_this.modules[m].type])
            }
        }
    }
    this.redraw=function() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        for (var l in _this.links) {
            ctx.beginPath();
            for (var L in _this.links[l]) {
                var mod=_this.modules[_this.links[l][L][0]],s=_this.links[l][L][1],x,y;
                if (s=='r') {
                    y=mod.y+(_this.links[l][L][2]*20)
                    x=mod.x+20
                } else if (s=='l') {
                    y=mod.y+(_this.links[l][L][2]*20)
                    x=mod.x-20
                } else if (s=='t') {
                    y=mod.y-10
                    x=mod.x
                }else if (s=='b') {
                    y=mod.y+30
                    x=mod.x
                }
                if (L) ctx.lineTo(x,y)
                else ctx.moveTo(x,y)
            }
            ctx.stroke();
        }
        for (var m in _this.modules) {
            _this.modules[m].draw()
        }
    }
    this.getLink=function(link) {
        for (var l in _this.links) {
            for (var L in _this.links[l]) {
                var match=0,
                    r;
                for (var j in _this.links[l][L]) {
                    if (_this.links[l][L][j]==link[j]) match++
                }
                if (L) r=0
                else r=1
                if (match==3) {
                    if (_this.modules[_this.links[l][r][0]].type=='N') {
                        return _this.getLink([_this.links[l][r][0],'r',0])//_this.links[l][r]
                    } else {
                        return _this.links[l][r]
                    }
                } 
            }
        }
        return false
    }
    this.build();
};
if(localStorage.parts==undefined){
    window.save_parts=[]
} else{
    window.save_parts=JSON.parse(localStorage.parts);
}
parts_list();