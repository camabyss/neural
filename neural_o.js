function Neural_o(SET) {
    var _this=this
	this.ends={'in':[],'out':[]}
	this.construct=function () {
		this.NEURO=new Neuro('new',true,$('#contain'))
		this.NEURO.run_limit=100
		for (var i in SET['in'][0]) {
			this.ends['in'].push(this.NEURO.neurons.length)
			this.NEURO.add_neuron('O')()
		}
		for (var o in SET['out'][0]) {
			this.ends['out'].push(this.NEURO.neurons.length)
			this.NEURO.add_neuron('R')()
		}
	}
	this.revisions=function (max_revs) {
	    _this.done=false
		    
		var count=0,
			exists=false,
			tests=[];
		for (var sp in window.save_parts) {
			if (window.save_parts[sp].name=='sys_'+SET['name']) {
				this.NEURO.load_part(sp)()
				exists=true
				break;
			}
		}
		console.log('-------------revisions')
		while (!exists && !_this.done && count<max_revs) {
			this.revision(this.decide())
			var tested=this.test()
			if (tested.count==SET.out.length*SET.out[0].length) {
			    _this.done=true
			}
			else _this.done=false
			tests.push(tested)
			//console.log('-----: '+count)
			//console.log(clone(this.NEURO.neurons))
			//console.log(clone(this.NEURO.links))
			//console.log(this.NEURO.neurons.length+':'+this.NEURO.links.length)
			count++
		}
		
		if (_this.done || exists) {
		console.log('-----------------clean up')
			//cleanup
			//display
			this.NEURO.part_name='sys_'+SET['name']
			//this.NEURO.save()
			//window.NEURO.parts_list()
			//cleanup by revision...
			//return this.NEURO
    		/*console.log('------------neurons')
    		console.log(clone(this.NEURO.neurons))
			
			for (var n=this.NEURO.neurons.length-1;n>=0;n--) {
			    var dec=this.decide({'kind':'neuron','action':'remove','reff':n})
			    console.log(dec)
				var UNDO=this.revision(dec)
				var tested=this.test()
				if (!tested) {
					this.undo(UNDO)
				}
			}
    		console.log(clone(this.NEURO.neurons))
    		console.log('-----------links')
    		console.log(clone(this.NEURO.links))
			for (var l=this.NEURO.links.length-1;l>=0;l--) {
			    var dec=this.decide({'kind':'link','action':'remove','reff':l})
				var UNDO=this.revision(dec)
				var tested=this.test()
				if (!tested) {
					this.undo(UNDO)
				}
			}
    		console.log(clone(this.NEURO.links))*/
			this.NEURO.save()
			window.NEURO.parts_list()
			return 'done'
			//return this.NEURO
		} else {
		    var bests=[]
		    for (var b=0;b<10;b++) {
		        bests.push({'count':0})
		    }
		    for (var t in tests) {
		        for (var b in bests) {
		            if (bests[b].count<tests[t].count) {
		                bests.splice(b,1,tests[t])
		                break;
		            }
		        }
		    }
			return bests
		}
	}
	this.test=function () {
		//ensure opens are connected
		for (var o in this.ends['in']) {
			var o_link=[this.ends['in'][o],'out',0]
			if (! this.NEURO.next_link(o_link)) {
				//new link
				return false
			}
		}
		//ensure returns are connected
		for (var r in this.ends['out']) {
			var r_link=[this.ends['out'][r],'in',0]
			
			if (! this.NEURO.prev_link(r_link)) {
				//new link
				return false
			}
		}
		// for each case
		_this.master_match=[]
		_this.count=0
		for (var i in SET['in']) {
			// set inputs
			for (var I in SET['in'][i]) {
				this.NEURO.neurons[this.ends['in'][I]].O=SET['in'][i][I]
			}
			var ran=this.NEURO.run(this.tested(i));
		}
		
	}
	this.TRY=function () {
		var t=false,
		c=0;
		while(!t ){//&& c<10) {
			 var rev=this.revisions(100)
			 //return to best
			 if (rev=='done') {
			     t=true
			     break;
			 }
			 else {
			   // console.log(rev)
			    var rev_list=[]
		        rev.sort(function(a, b) {
                    return a.count - b.count;
                })
                if (rev[0].count==0) {
                    //return false
                    t=false
                } else {
                    //reset to state
                    //console.log(rev[0])
                    this.NEURO.neurons=this.NEURO.load_neurons(rev[0]['state']['neurons'])
                    this.NEURO.links=rev[0]['state']['links']
                    //return false
                    t=false
                }
			 }
			 c++
		}
		console.log(c*100)
		return t
	}
	this.decide=function (force) {
		/* These are exactly the 
		kind of rules that could 
		be decided by the neurons */
		if (force===undefined) force={}
		var decision={'kind':false,'action':false,'reff':false}
		//force & decide kind
		for (var d in decision) {
			if (d in force){
				decision[d]=force[d]
			} else {
				if (d=='kind') {
					decision[d]=rand_i(['neuron','link'])
				} else if (d=='action') {
					decision[d]=rand_int(1,10)
				}
			}
		}
		
		//decide action
		if (decision['kind']=='link' && decision['action']==10) decision['action']='add'
		if (decision['action']==10 && this.NEURO.get_type('V').length==0) decision['action']='remove'
		if (decision['action']<=5) decision['action']='add'
		else if (decision['action']<=9) decision['action']='remove'
		else if (decision['action']==10) decision['action']='change'
		if (decision['kind']=='neuron') {
			if (decision['action']=='remove' && this.NEURO.neurons.length<(this.NEURO.get_type('O').length+this.NEURO.get_type('R').length+1)) {
				decision['action']=rand_int(1,10)
				if (decision['action']<3) decision['action']='remove'
				else decision['action']='add'
			}
			if (decision['action']=='add' && this.NEURO.neurons.length>20) {
				decision['action']=rand_int(1,10)
				if (decision['action']<3) decision['action']='add'
				else decision['action']='remove'
			}
			if (decision['action']=='remove' && this.NEURO.neurons.length==(this.NEURO.get_type('O').length+this.NEURO.get_type('R').length)) decision['action']='add'
		} else if (decision['kind']=='link') {
			if (decision['action']=='remove' && this.NEURO.links.length<(this.NEURO.neurons.length/2)) {
				decision['action']=rand_int(1,10)
				if (decision['action']<3) decision['action']='remove'
				else decision['action']='add'
			}
			if (decision['action']=='add' && this.NEURO.links.length>(this.NEURO.neurons.length*2)) {
				decision['action']=rand_int(1,10)
				if (decision['action']<3) decision['action']='add'
				else decision['action']='remove'
			}
			if (decision['action']=='remove' && this.NEURO.links.length==0) decision['action']='add'
		}
		
		//force again
// 		for (var d in decision) {
// 			if (d in force){
// 				decision[d]=force[d]
// 			}
// 		}

		//set reff
		if(!('reff' in force)){
    		if (decision['kind']=='link') {
    			//if add
    			if (decision['action']=='add') {
    				//choose output from empty
    				var out_link=[parseInt(this.ends['in'][0]),'out',0]
    				var out_links=[]
    				for (var n in this.NEURO.neurons) {
    					for (var e in this.NEURO.neurons[n].ends['out']) {
    						var nl=this.NEURO.next_link([parseInt(n),'out',parseInt(e)]);
    						if (! nl) {
    							out_links.push([parseInt(n),'out',parseInt(e)])
    							
    						}
    					}
    				}
    				out_link=rand_i(out_links)
    				//choose input from any
    				var in_link=[parseInt(this.ends['out'][0]),'in',0]
    				var in_links=[]
    				for (var n in this.NEURO.neurons) {
    					for (var e in this.NEURO.neurons[n].ends['in']) {
    						in_links.push([parseInt(n),'in',parseInt(e)])
    					}
    				}
    				in_link=rand_i(in_links)
    				//add
    				decision['reff']=[in_link,out_link]
    			//else
    			} else if (decision['action']=='remove') {
    				//select random
    				decision['reff']=rand_int(0,this.NEURO.links.length-1)
    				//remove
    			}
    		//if neuron
    		} else if (decision['kind']=='neuron') {
    			//if add
    			if (decision['action']=='add') {
    				//add random (not O/R)
    				var types=[]
    				for (var v in window.neuron_defs) {
    					if (v!='O'&&v!='R'&&v!='N') {
    						types.push(v)
    					}
    				}
    				decision['reff']=rand_i(types)
    			//if remove
    			} else if (decision['action']=='remove') {
    				//select neuron (not O/R)
    				var neurons=[]
    				for (var n in this.NEURO.neurons){
    					var neuron=this.NEURO.neurons[n]
    					if(neuron.type!='O'&&neuron.type!='R'){
    						neurons.push(n)
    					}
    				}
    				//remove
    				if (neurons.length) {
    					decision['reff']=rand_i(neurons)
    				}
    			//if change
    			} else if (decision['action']=='change') {
    				//select neuron (V)
    				var vs=this.NEURO.get_type('V'),
    					n=rand_i(vs);
    				decision['reff']=n
    				decision['value']=rand_int(0,1)
    			}
    		}
		}
		
		
		return decision
	}
	this.revision=function (decision) {
	    //console.log(decision)
		var un_decision={}
		if (decision['kind']=='link') {
			//if add
			if (decision['action']=='add') {
				un_decision={'action':'remove','kind':'link','reff':this.NEURO.links.length}
				this.NEURO.links.push(decision['reff'])
			//else
			} else if (decision['action']=='remove') {
				//select random
				var reff=this.NEURO.links.splice(decision['reff'],1)
				un_decision={'action':'add','kind':'link','reff':reff}
				//remove
			}
		//if neuron
		} else if (decision['kind']=='neuron') {
			//if add
			if (decision['action']=='add') {
				//add random (not O/R)
				un_decision={'action':'remove','kind':'neuron','reff':this.NEURO.neurons.length}
				this.NEURO.add_neuron(decision['reff'])()
				if ('value' in decision) this.NEURO.neurons[this.NEURO.neurons.length-1].O=decision['value']
			//if remove
			} else if (decision['action']=='remove') {
				//select neuron (not O/R)
				// this is the hard part, get links as well as neuron
				un_decision={'action':'add','kind':'neuron','reff':this.NEURO.neurons[decision['reff']].type,'links':[]}
				if (this.NEURO.neurons[decision['reff']].type=='O' || this.NEURO.neurons[decision['reff']].type=='V') un_decision['value']=this.NEURO.neurons[decision['reff']].O
				for (var e in this.NEURO.neurons[decision['reff']].ends['in']) {
					var pl=this.NEURO.prev_link([parseInt(decision['reff']),'in',parseInt(e)])
					if (pl) un_decision['links'].push([['in',e],pl])
				}
				for (var e in this.NEURO.neurons[decision['reff']].ends['out']) {
					var nl=this.NEURO.next_link([parseInt(decision['reff']),'out',parseInt(e)])
					if (nl) un_decision['links'].push([nl,['out',e]])
				}
				this.NEURO.remove_neuron(decision['reff'])
			//if change
			} else if (decision['action']=='change') {
				//select neuron (V)
				un_decision={'action':'change','kind':'neuron','reff':decision['reff'],'value':this.NEURO.neurons[decision['reff']].O}
				this.NEURO.neurons[decision['reff']].O=decision['value']
			}
		}
		return un_decision
		//return [undo]
	}
	this.undo=function (un_decision) {
	    if (un_decision['kind']=='neuron' && un_decision['action']=='add') {
	        var n=this.NEURO.neurons.length
	        this.revision(un_decision)
	        for (var l in un_decision['links']) {
	            var link=un_decision['links'][l]
	            for (var L in un_decision['links'][l]) {
	                if (un_decision['links'][l][L].length==2) {
	                    link[L].unshift(n)
	                }
	            }
	            this.NEURO.links.push(link)
	        }
	    } else {
	        this.revision(un_decision)
	    }
	}
	//event driven
	this.clean_up=function() {
	    
	}
	this.next_rev=function(){
	    this.revision(this.decide())
		var tested=this.test()
		if (tested.count==SET.out.length*SET.out[0].length) {
		    _this.done=true
		}
		else _this.done=false
		tests.push(tested)
	}
	this.tested=function(i) {
	    return function (ran) {
    	    var match=0;
    			for (var j in SET['out'][i]) {
    				if (ran[j]==SET['out'][i][j]) match++
    			}
    			_this.count+=match
    			_this.master_match.push(match)
    			//if (match==SET['out'][i].length) master_match++
    		}
    		if (_this.master_match.length==SET['in'].length) _this.next_rev()
    		
    // 		return {'matches':_this.master_match,'count':_this.count,'state':{
    // 		    'neurons':this.NEURO.save_neurons(),
    // 		    'links':clone(this.NEURO.links)
    // 		}}
	    //}
	}
	
	this.construct()
}